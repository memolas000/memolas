<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlideTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slide', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('title', 255)->nullable();
            $table->text('short_description')->nullable();
            $table->text('image');
            $table->text('url')->nullable();
            $table->text('facebook_url')->nullable();
            $table->text('instal_url')->nullable();
            $table->integer('sort');
            $table->text('title_seo')->nullable();
            $table->integer('category_id');
            $table->tinyInteger('status');
            $table->tinyInteger('user_id');
            $table->longText('content')->nullable();
            $table->longText('content_seo')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slide');
    }
}
