<?php


namespace App\Scopes;


trait BaseScope
{
    public function scopeWhereLike($query, $searchField, $searchKey)
    {
        if (!isset($searchKey) || $searchKey == null) {
            $searchKey = "";
        }
        $searchKey = str_replace('%', '\%', $searchKey);
        return $query->Where($searchField, 'like', '%' . $searchKey . '%');
    }
}