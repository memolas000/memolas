<?php

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

if (!function_exists('isManager')) {
	function isManager()
	{
		return request()->route()->getPrefix() == 'admin';
	}
}

function getPerPage()
{
	return request()->has('perPage') ? request()->query('perPage') : config('const.paging.default');
}

function showPassword($password = '')
{
	if (empty($password)) {
		return;
	}

	return str_repeat("●", mb_strlen($password));
}

function formatDateTime($date)
{
    if (empty($date)){
        return null;
    }
    return Carbon::parse($date)->format('Y/m/d H:i');
}

function formatLimitDate($date)
{
    return Carbon::parse($date)->format('Y/m/d');
}

function checkCurrentMenu($controller, $action, $menuController, $menuAction)
{
    if (empty($controller) || empty($action) || empty($menuAction) || empty($menuController) || $controller != $menuController || !is_array($menuAction)) {
        return null;
    }
    return in_array($action, array_map('strtolower', $menuAction)) ? 'active' : '';
}

function oldData($data, $field, $defaultValue = '') {
	return old($field, isset($data[$field]) ? $data[$field] : $defaultValue);
}

function getApprovalStatus($approval_status)
{
    switch ($approval_status) {
        case  config('const.approval_status.unapproved'):
            return config('messages.approval_status.unapproved');
            break;
        case  config('const.approval_status.approved'):
            return config('messages.approval_status.approved');
            break;
        case  config('const.approval_status.send_off'):
            return config('messages.approval_status.send_off');
            break;
    }
}

function getClassApprovalStatus($approval_status)
{
    switch ($approval_status) {
        case  config('const.approval_status.unapproved'):
            return 'warning';
            break;
        case  config('const.approval_status.approved'):
            return 'success';
            break;
        case  config('const.approval_status.send_off'):
            return 'secondary';
            break;
    }
}

function getBankAccountType($type)
{
    $data = [
        config('const.bank_account_type.normal') => config('messages.bank_account_type.normal'),
        config('const.bank_account_type.current') => config('messages.bank_account_type.current')
    ];

    return !empty($data[$type]) ? $data[$type] : '';
}
/**
 * @param $point
 * @return array
 * Convert filed in workers save string format 'a,b,c' to array
 */
function convertPointToArray($point)
{
    if (empty($point)) {
        return [];
    }
    if (!is_array($point) && strpos($point, ',') === false) { // Check if has ','
        return [$point];
    }

    return !is_array($point) ? explode(',', $point) : $point;
}

/**
 * @param $field
 * @param $data
 * @return array|mixed
 * Check valid worker edit/resgister if error
 */
function checkDataRollBack($field, $data)
{
    if (empty($field)) {
        return [];
    }
    $workTypeData = oldData($data, $field, '');
    $workTypeData = convertPointToArray($workTypeData);
    return $workTypeData;
}

function convertFileSize($size)
{
    if (!is_numeric($size) || $size < 0) {
        return false;
    }

    switch ($size) {
        case $size > pow(1024, 3) : // GB
            $convertedSize = $size / pow(1024, 3);
            return sprintf("%s %s", number_format($convertedSize, 2), "GB");
        case  $size > pow(1024, 2):
            $convertedSize = $size / pow(1024, 2);
            return sprintf("%s %s", number_format($convertedSize, 2), "MB");
        case  $size > 1024:
            $convertedSize = $size / 1024;
            return sprintf("%s %s", number_format($convertedSize, 2), "KB");
        case  $size > 1:
            return sprintf("%s %s", $size, "bytes");
        case  $size >= 0:
            return sprintf("%s %s", $size, "byte");
        default:
            return false;
    }
}

function shortenFileName($fileName){
    if(strlen($fileName) > 100 ){
        $fileName = substr($fileName, 0, 100). strrchr($fileName, ".");
    }
    return $fileName;
}

/**
 * @param $cateId
 */
function getCategory($cateId)
{
    $data = config('const.slide_category');
    return !empty($cateId) ? $data[$cateId] : '';
}


/**
 * @param $cateId
 */
function getCategoryPost($cateId)
{
    $data = config('const.posts_category');
    return !empty($cateId) ? $data[$cateId] : '';
}

/**
 * @param $optionId
 */
function getOptionType($optionId)
{
    $data = config('const.option_type');
    return !empty($optionId) ? $data[$optionId] : '';
}