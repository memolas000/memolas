<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory;

class SlideRequest extends FormRequest
{

    public function __construct(Factory $validationFactory)
    {
        $validationFactory->extend(
            'extension',
            function ($attribute, $value, $parameters) {
                if (empty($value)) {return false;}
                $explode = explode('.',$value);
                if (empty($explode) && !isset($explode[1])) {return false;}
                if (in_array($explode[1], ['jpeg','jpg','png'])) {return true;}
                return false;
            }
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param null $id
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'sometimes|nullable|max:255',
            'image' => 'required|extension',
            'url' => 'sometimes|nullable|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'sort' => 'required|regex:/^[0-9]*$/',
            'category_id' => 'required|regex:/^[0-9]*$/',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'title.max' => 'Tiêu đề không được quá 255 ký',
            'image.required' => 'Ảnh không được để trống',
            'image.extension' => 'Avatar chỉ chấp nhận đuôi jpeg, png, jpg',
            'url.regex' => 'Url không đúng định dạng',
            'sort.required' => 'Thứ tự hiển thị trên trang không được phép trống',
            'sort.regex' => 'Thứ tự hiển thị trên trang phải là số',
            'category_id.required' => 'Bạn phải chọn vị trí hiển thị trên trang',
            'category_id.regex' => 'Vị trí phải là số ',
        ];
    }
}
