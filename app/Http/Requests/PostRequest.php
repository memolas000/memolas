<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function __construct(Factory $validationFactory)
    {
        $validationFactory->extend(
            'extension',
            function ($attribute, $value, $parameters) {
                if (empty($value)) {return false;}
                $explode = explode('.',$value);
                if (empty($explode) && !isset($explode[1])) {return false;}
                if (in_array($explode[1], ['jpeg','jpg','png'])) {return true;}
                return false;
            }
        );
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @param null $id
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'title_seo' => 'required',
            'avatar_post' => 'sometimes|nullable|extension',
            'short_description' => 'max:256',
            'content' => 'required',
            'category_id' => 'required',
            'status' => 'required',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'title.required' => 'Tiêu đề không được bỏ trống',
            'title_seo.required' => 'Tiêu đề không được bỏ trống',
            'avatar_post.required' => 'Ảnh không được để trống',
            'avatar_post.extension' => 'Avatar chỉ chấp nhận đuôi jpeg, png, jpg',
            'short_description.max' => 'Mô tả ngắn chỉ chứa tối đa 256 ký tự',
            'content.required' => 'Nội dung không được phép để chống',
            'category_id.required' => 'Bạn phải chọn vị trí hiển thị trên trang',
        ];
    }
}
