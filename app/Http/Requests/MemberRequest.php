<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory;

class MemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function __construct(Factory $validationFactory)
    {
        $validationFactory->extend(
            'extension',
            function ($attribute, $value, $parameters) {
                if (empty($value)) {return false;}
                $explode = explode('.',$value);
                if (empty($explode) && !isset($explode[1])) {return false;}
                if (in_array($explode[1], ['jpeg','jpg','png'])) {return true;}
                return false;
            }
        );
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @param null $id
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:255',
            'job' => 'required|max:255',
            'avatar' => 'required|extension',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên thành viên không được bỏ trống',
            'job.required' => 'Công việc không được bỏ trống',
            'name.max' => 'Tên thành viên chỉ chứa tối đa 255 ký tự',
            'job.max' => 'Công việc chỉ chứa tối đa 255 ký tự',
        ];
    }
}
