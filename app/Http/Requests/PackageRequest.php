<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory;

class PackageRequest extends FormRequest
{

    public function __construct(Factory $validationFactory)
    {
        $validationFactory->extend(
            'extension',
            function ($attribute, $value, $parameters) {
                if (empty($value)) {return false;}
                $explode = explode('.',$value);
                if (empty($explode) && !isset($explode[1])) {return false;}
                if (in_array($explode[1], ['jpeg','jpg','png'])) {return true;}
                return false;
            }
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param null $id
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:255',
            'avatar' => 'required|extension',
            'price_fix' => 'required|regex:/^[0-9]*$/',
            'ratio_memolas' => 'required|regex:/^[0-9]*$/',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Tiêu đề không được phép trốngratio_memolas',
            'name.max' => 'Tiêu đề không được quá 255 ký',
            'avatar.required' => 'Ảnh không được để trống',
            'avatar.extension' => 'Avatar chỉ chấp nhận đuôi jpeg, png, jpg',
            'price_fix.required' => 'Giá cứng không được phép trống',
            'price_fix.regex' => 'Giá cứng phải là số',
            'ratio_memolas.required' => 'Tỷ lệ không được phép trống!',
            'ratio_memolas.regex' => 'Tỷ lệ phải là số!',
        ];
    }
}
