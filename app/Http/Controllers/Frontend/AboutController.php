<?php

namespace App\Http\Controllers\Frontend;

use App\Repositories\PostRepository;
use App\Repositories\SlideRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public $postRepository;
    public $slideRepository;

    public function __construct(PostRepository $postRepository, SlideRepository $slideRepository)
    {
        $this->postRepository = $postRepository;
        $this->slideRepository = $slideRepository;
    }

    public function index()
    {
        $introduce = $this->postRepository->getPostByType(config('const.posts_category_key.introduce'));
        $introducePopup = $this->postRepository->getPostByType(config('const.posts_category_key.introduce_popup'));
        $slideSubjectLoves = $this->slideRepository->getSlideByType(config('const.slide_category_key.subject_love'));
        $slideIntroduce = $this->slideRepository->getSlideByType(config('const.slide_category_key.popup_intro'));
        return view('frontend.about', compact('introduce', 'slideSubjectLoves', 'slideIntroduce' , 'introducePopup'));
    }
}
