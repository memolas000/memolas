<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\PostRepository;
use App\Repositories\SlideRepository;


class YearbookController extends Controller
{
    public $postRepository;
    public $slideRepository;

    public function __construct(PostRepository $postRepository, SlideRepository $slideRepository)
    {
        $this->postRepository = $postRepository;
        $this->slideRepository = $slideRepository;
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function main()
    {
        $introduce = $this->postRepository->getPostByType(config('const.posts_category_key.introduce'));
        $introducePopup = $this->postRepository->getPostByType(config('const.posts_category_key.introduce_popup'));
        $slideSubjectLoves = $this->slideRepository->getSlideByType(config('const.slide_category_key.subject_love'));
        $slideIntroduce = $this->slideRepository->getSlideByType(config('const.slide_category_key.popup_intro'));

        return view('frontend.yearbook' , compact('introduce', 'slideSubjectLoves', 'slideIntroduce' , 'introducePopup'));
    }
}
