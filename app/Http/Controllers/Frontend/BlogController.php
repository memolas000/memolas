<?php

namespace App\Http\Controllers\Frontend;

use App\Repositories\PostRepository;
use App\Services\PostService;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    /**
     * @var PostService
     */
    public $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts = $this->postRepository->getListPostByType(config('const.posts_category_key.blog'));
    
        return view('frontend.blog', compact('posts'));
    }
}
