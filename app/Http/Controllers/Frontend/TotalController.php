<?php

namespace App\Http\Controllers\Frontend;

use App\Repositories\OptionRepository;
use App\Repositories\PackageOptionRelateRepository;
use App\Repositories\PackageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TotalController extends Controller
{
    public $packageRepository;
    public $optionRepository;
    public $packageOptionRelateRepository;

    public function __construct(PackageRepository $packageRepository, OptionRepository $optionRepository,
                                PackageOptionRelateRepository $packageOptionRelateRepository)
    {
        $this->packageRepository = $packageRepository;
        $this->optionRepository = $optionRepository;
        $this->packageOptionRelateRepository = $packageOptionRelateRepository;
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function main()
    {
        $package = $this->packageRepository->getList();
        $option = $this->optionRepository->getListOptionByCate(config('const.option_category_key.material'));
        $optionPackage = $this->optionRepository->getAllPackageOption();
        return view('frontend.total',compact('package', 'option', 'optionPackage'));
    }
}
