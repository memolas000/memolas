<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PostRequest;
use App\Repositories\PostRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Repositories\CategoryBlogRepository;

class PostController extends Controller
{
    public $postRepository;
    public $categoryBlogRepository;

    public function __construct(PostRepository $postRepository, CategoryBlogRepository $categoryBlogRepository)
    {
        $this->postRepository = $postRepository;
        $this->categoryBlogRepository = $categoryBlogRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->postRepository->getList();
        return view('admin.posts.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        $category = config('const.posts_category');
        $postsStatus = config('const.posts_status');
        $categoryBlog = $this->categoryBlogRepository->getList();
        return view('admin.posts._form', compact('data', 'category', 'postsStatus', 'categoryBlog'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $data = $request->all();
        try {
            $data['content_seo'] = $data['content'];
            $data['content_seo_2'] = $data['content_2'];
            $data['short_description'] = trim($data['short_description']);
            $id = $this->postRepository->create($data);
            if($data['status'] == config('const.posts_status_key.enable') && $data['category_id'] != config('const.posts_category_key.blog')) {
                $this->postRepository->updateAllStatus($id, $data['category_id']);
            }
            return redirect()->route('admin.posts.index')->with('success', config('messages.DB_INSERT'));
        } catch (\Exception $e) {
            Log::error('Create fail !', ['error' => $e->getMessage()]);
        }
        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->postRepository->detail($id);
        return view('admin.posts.detail', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (empty($id)){
            abort(404);
        }
        $data = $this->postRepository->detail($id);
        $category = config('const.posts_category');
        $postsStatus = config('const.posts_status');
        $categoryBlog = $this->categoryBlogRepository->getList();
        return view('admin.posts._form', compact('data', 'category', 'postsStatus', 'id', 'categoryBlog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        $post = $request->all();
        if (empty($id) || empty($post)) {
            abort(404);
        }
        try {
            $post['content_seo'] = $post['content'];
            $post['content_seo_2'] = $post['content_2'];
            $post['short_description'] = trim($post['short_description']);
            $this->postRepository->update($id, $post);
            if($post['status'] == config('const.posts_status_key.enable') && $post['category_id'] != config('const.posts_category_key.blog')) {
                $this->postRepository->updateAllStatus($id, $post['category_id']);
            }
            return redirect(route('admin.posts.index'));
        } catch (\Exception $e) {
            Log::error('Update fail !', ['error' => $e->getMessage()]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (empty($id)) {
            return $this->responErrorAjax("Xóa không thành công");
        }
        try {
            $this->postRepository->softDelete($id);
            \request()->session()->flash('status', 'Xóa thành công!');
            return $this->responSuccessAjax(array(), "Xóa thành công");
        } catch (\Exception $e) {
            \request()->session()->flash('status', 'Xóa không thành công!');
            return $this->responErrorAjax("Xóa không thành công");
        }

    }

    public function uploadFile(Request $request)
    {
        if ($request->ajax()) {
            $sourcePath = $_FILES['fileToUpload']['tmp_name'];       // Storing source path of the file in a variable
            $targetPath = public_path() . "/upload/posts";
            if (!file_exists($targetPath)) {
                if (!mkdir($targetPath, 0777, true)) {
                    echo "Check sevice! khong co quyen tao file thu muc";
                }
            }
            $nameRaw = $_FILES['fileToUpload']['name'];
            if (empty($nameRaw)){
                exit();
            }
            $nameRaw = explode('.', $nameRaw);
            $name = !empty($nameRaw[0]) ? $nameRaw[0] : '';
            $ext = !empty($nameRaw[1]) ? $nameRaw[1] : '';
            $fullName =  $name . time() . rand(1, 9999) .'.'. $ext;
            $targetPath = $targetPath . '/' . $fullName;
            if (move_uploaded_file($sourcePath, $targetPath)) {// Moving Uploaded file
                echo $fullName;
            }
        }
        exit();
    }

    public function responErrorAjax($message, $status = 400, $header = 'application/json')
    {
        $data = [
            'success' => false,
            'message' => $message
        ];
        return response($data, $status)
            ->header('Content-Type', $header);
    }

    public function responSuccessAjax($data=array(), $message, $status = 200, $header = 'application/json')
    {
        if (empty($data)){
            $data = [
                'success' => true,
                'message' => $message
            ];
        }
        return response($data, $status)
            ->header('Content-Type', $header);
    }
}
