<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PackageRequest;
use App\Repositories\OptionRepository;
use App\Repositories\PackageOptionRelateRepository;
use App\Repositories\PackageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class PackageController extends Controller
{
    public $packageRepository;
    public $optionRepository;
    public $packageOptionRelateRepository;

    public function __construct(PackageRepository $packageRepository, OptionRepository $optionRepository,
                                PackageOptionRelateRepository $packageOptionRelateRepository)
    {
        $this->packageRepository = $packageRepository;
        $this->optionRepository = $optionRepository;
        $this->packageOptionRelateRepository = $packageOptionRelateRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->packageRepository->getList();
        return view('admin.package.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        $packageStatus = config('const.package_status');
        $option = $this->optionRepository->getList();
        if(!empty($option)){
            $option = $option->toArray();
        }
        session()->put('option', $option);
        return view('admin.package._form', compact('data', 'packageStatus', 'option'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illumina te\Http\Response
     */
    public function store(PackageRequest $request)
    {
        $data = $request->all();
        $optionRelate = [];
        try {
            $packageId = $this->packageRepository->create($data);
            foreach ($data['price'] as $optionId => $price){
                $optionRow['option_id'] = $optionId;
                $optionRow['package_id'] = $packageId;
                $optionRow['price'] = $price;
                $optionRow['min_price'] = !empty($data['min_price'][$optionId]) ? $data['min_price'][$optionId] :0;
                $optionRow['created_at'] = date('Y-m-d H:i:s');
                $optionRelate[] = $optionRow;
            }
            $this->packageOptionRelateRepository->createMulti($optionRelate);
            return redirect()->route('admin.package.index')->with('success', config('messages.DB_INSERT'));
        } catch (\Exception $e) {
            Log::error('Create fail !', ['error' => $e->getMessage()]);
        }
        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->packageRepository->detail($id);
        $option = $this->optionRepository->getList();
        $optionPackage = $this->optionRepository->getPackageOption($id);
        if(!empty($option)){
            $option = $option->toArray();
        }
        if(!empty($optionPackage)){
            $optionPackage = $optionPackage->toArray();
        }
        return view('admin.package.detail', compact('data' , 'option' ,'optionPackage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (empty($id)){
            abort(404);
        }
        $data = $this->packageRepository->detail($id);
        $option = $this->optionRepository->getList();
        $optionPackage = $this->optionRepository->getPackageOption($id);
        if(!empty($option)){
            $option = $option->toArray();
        }
        if(!empty($optionPackage)){
            $optionPackage = $optionPackage->toArray();
        }
        $packageStatus = config('const.package_status');
        return view('admin.package._form', compact('data', 'packageStatus', 'id', 'optionPackage','option'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PackageRequest $request, $id)
    {
        $post = $request->all();
        if (empty($id) || empty($post)) {
            abort(404);
        }
        try {
//            $this->packageRepository->update($id, $post);
            foreach ($post['price'] as $optionId => $price){
                $data = $this->packageOptionRelateRepository->findByKey($id, $optionId);
                $optionRow['price'] = $price;
                $optionRow['min_price'] = !empty($post['min_price'][$optionId]) ? $post['min_price'][$optionId] :0;
                $optionRow['package_id'] = $id;
                $optionRow['option_id'] = $optionId;
                if (!empty($data['id'])) {
                    $this->packageOptionRelateRepository->update($data['id'], $optionRow);
                }else{
                    $this->packageOptionRelateRepository->create($optionRow);
                }
            }
            return redirect(route('admin.package.index'));
        } catch (\Exception $e) {
            Log::error('Update fail !', ['error' => $e->getMessage()]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (empty($id)) {
            return $this->responErrorAjax("Xóa không thành công");
        }
        try {
            $this->packageRepository->softDelete($id);
            \request()->session()->flash('status', 'Xóa thành công!');
            return $this->responSuccessAjax(array(), "Xóa thành công");
        } catch (\Exception $e) {
            \request()->session()->flash('status', 'Xóa không thành công!');
            return $this->responErrorAjax("Xóa không thành công");
        }

    }

    public function uploadFile(Request $request)
    {
        if ($request->ajax()) {
            $sourcePath = $_FILES['fileToUpload']['tmp_name'];       // Storing source path of the file in a variable
            $targetPath = public_path() . "/upload/package";
            if (!file_exists($targetPath)) {
                if (!mkdir($targetPath, 0777, true)) {
                    echo "Check sevice! khong co quyen tao file thu muc";
                }
            }
            $nameRaw = $_FILES['fileToUpload']['name'];
            if (empty($nameRaw)){
                exit();
            }
            $nameRaw = explode('.', $nameRaw);
            $name = !empty($nameRaw[0]) ? $nameRaw[0] : '';
            $ext = !empty($nameRaw[1]) ? $nameRaw[1] : '';
            $fullName =  $name . time() . rand(1, 9999) .'.'. $ext;
            $targetPath = $targetPath . '/' . $fullName;
            if (move_uploaded_file($sourcePath, $targetPath)) {// Moving Uploaded file
                echo $fullName;
            }
        }
        exit();
    }

    public function setOption(Request $request)
    {
        if ($request->ajax()) {

        }
        exit();
    }

    public function responErrorAjax($message, $status = 400, $header = 'application/json')
    {
        $data = [
            'success' => false,
            'message' => $message
        ];
        return response($data, $status)
            ->header('Content-Type', $header);
    }

    public function responSuccessAjax($data=array(), $message, $status = 200, $header = 'application/json')
    {
        if (empty($data)){
            $data = [
                'success' => true,
                'message' => $message
            ];
        }
        return response($data, $status)
            ->header('Content-Type', $header);
    }
}
