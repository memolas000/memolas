<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MemberRequest;
use App\Repositories\MemberRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class MemberController extends Controller
{
    public $memberRepository;

    public function __construct(MemberRepository $memberRepository)
    {
        $this->memberRepository = $memberRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->memberRepository->getList();
        return view('admin.member.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        $memberStatus = config('const.member_status');
        return view('admin.member._form', compact('data', 'memberStatus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(MemberRequest $request)
    {
        $data = $request->all();
        try {
            $this->memberRepository->create($data);
            return redirect()->route('admin.member.index')->with('success', config('messages.DB_INSERT'));
        } catch (\Exception $e) {
            Log::error('Create fail !', ['error' => $e->getMessage()]);
        }
        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->memberRepository->detail($id);
        return view('admin.member.detail', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (empty($id)){
            abort(404);
        }
        $data = $this->memberRepository->detail($id);
        $memberStatus = config('const.member_status');
        return view('admin.member._form', compact('data', 'memberStatus', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(MemberRequest $request, $id)
    {
        $post = $request->all();
        if (empty($id) || empty($post)) {
            abort(404);
        }
        try {
            $this->memberRepository->update($id, $post);
            return redirect(route('admin.member.index'));
        } catch (\Exception $e) {
            Log::error('Update fail !', ['error' => $e->getMessage()]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (empty($id)) {
            return $this->responErrorAjax("Xóa không thành công");
        }
        try {
            $this->memberRepository->softDelete($id);
            \request()->session()->flash('status', 'Xóa thành công!');
            return $this->responSuccessAjax(array(), "Xóa thành công");
        } catch (\Exception $e) {
            \request()->session()->flash('status', 'Xóa không thành công!');
            return $this->responErrorAjax("Xóa không thành công");
        }

    }

    public function uploadFile(Request $request)
    {
        if ($request->ajax()) {
            $sourcePath = $_FILES['fileToUpload']['tmp_name'];       // Storing source path of the file in a variable
            $targetPath = public_path() . "/upload/member";
            if (!file_exists($targetPath)) {
                if (!mkdir($targetPath, 0777, true)) {
                    echo "Check sevice! khong co quyen tao file thu muc";
                }
            }
            $nameRaw = $_FILES['fileToUpload']['name'];
            if (empty($nameRaw)){
                exit();
            }
            $nameRaw = explode('.', $nameRaw);
            $name = !empty($nameRaw[0]) ? $nameRaw[0] : '';
            $ext = !empty($nameRaw[1]) ? $nameRaw[1] : '';
            $fullName =  $name . time() . rand(1, 9999) .'.'. $ext;
            $targetPath = $targetPath . '/' . $fullName;
            if (move_uploaded_file($sourcePath, $targetPath)) {// Moving Uploaded file
                echo $fullName;
            }
        }
        exit();
    }

    public function responErrorAjax($message, $status = 400, $header = 'application/json')
    {
        $data = [
            'success' => false,
            'message' => $message
        ];
        return response($data, $status)
            ->header('Content-Type', $header);
    }

    public function responSuccessAjax($data=array(), $message, $status = 200, $header = 'application/json')
    {
        if (empty($data)){
            $data = [
                'success' => true,
                'message' => $message
            ];
        }
        return response($data, $status)
            ->header('Content-Type', $header);
    }
}
