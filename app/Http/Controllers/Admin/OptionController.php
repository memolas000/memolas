<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OptionRequest;
use App\Repositories\OptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class OptionController extends Controller
{
    public $optionRepository;

    public function __construct(OptionRepository $optionRepository)
    {
        $this->optionRepository = $optionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->optionRepository->getList();
        return view('admin.option.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        $optionStatus = config('const.option_status');
        $optionType = config('const.option_type');
        return view('admin.option._form', compact('data', 'optionStatus', 'optionType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(OptionRequest $request)
    {
        $data = $request->all();
        try {
            $this->optionRepository->create($data);
            return redirect()->route('admin.option.index')->with('success', config('messages.DB_INSERT'));
        } catch (\Exception $e) {
            Log::error('Create fail !', ['error' => $e->getMessage()]);
        }
        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->optionRepository->detail($id);
        return view('admin.option.detail', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (empty($id)){
            abort(404);
        }
        $data = $this->optionRepository->detail($id);
        $optionStatus = config('const.option_status');
        $optionType = config('const.option_type');
        return view('admin.option._form', compact('data', 'optionStatus', 'id', 'optionType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(OptionRequest $request, $id)
    {
        $post = $request->all();
        if (empty($id) || empty($post)) {
            abort(404);
        }
        try {
            $this->optionRepository->update($id, $post);
            return redirect(route('admin.option.index'));
        } catch (\Exception $e) {
            Log::error('Update fail !', ['error' => $e->getMessage()]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (empty($id)) {
            return $this->responErrorAjax("Xóa không thành công");
        }
        try {
            $this->optionRepository->softDelete($id);
            \request()->session()->flash('status', 'Xóa thành công!');
            return $this->responSuccessAjax(array(), "Xóa thành công");
        } catch (\Exception $e) {
            \request()->session()->flash('status', 'Xóa không thành công!');
            return $this->responErrorAjax("Xóa không thành công");
        }

    }

    public function responErrorAjax($message, $status = 400, $header = 'application/json')
    {
        $data = [
            'success' => false,
            'message' => $message
        ];
        return response($data, $status)
            ->header('Content-Type', $header);
    }

    public function responSuccessAjax($data=array(), $message, $status = 200, $header = 'application/json')
    {
        if (empty($data)){
            $data = [
                'success' => true,
                'message' => $message
            ];
        }
        return response($data, $status)
            ->header('Content-Type', $header);
    }
}
