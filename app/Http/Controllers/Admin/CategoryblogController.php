<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CategoryBlogRequest;
use App\Repositories\CategoryBlogRepository;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class CategoryblogController extends Controller
{
    public $categoryBlogRepository;

    public function __construct(CategoryBlogRepository $categoryBlogRepository)
    {
        $this->categoryBlogRepository = $categoryBlogRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->categoryBlogRepository->getList();
        return view('admin.category_blog.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        $categoryBlogStatus = config('const.category_blog_status');
        return view('admin.category_blog._form', compact('data', 'categoryBlogStatus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryBlogRequest $request)
    {
        $data = $request->all();
        try {
            $this->categoryBlogRepository->create($data);
            return redirect()->route('admin.category-blog.index')->with('success', config('messages.DB_INSERT'));
        } catch (\Exception $e) {
            Log::error('Create fail !', ['error' => $e->getMessage()]);
        }
        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->categoryBlogRepository->detail($id);
        return view('admin.category_blog.detail', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (empty($id)){
            abort(404);
        }
        $data = $this->categoryBlogRepository->detail($id);
        $categoryBlogStatus = config('const.category_blog_status');
        return view('admin.category_blog._form', compact('data', 'categoryBlogStatus', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryBlogRequest $request, $id)
    {
        $post = $request->all();
        if (empty($id) || empty($post)) {
            abort(404);
        }
        try {
            $this->categoryBlogRepository->update($id, $post);
            return redirect(route('admin.category-blog.index'));
        } catch (\Exception $e) {
            Log::error('Update fail !', ['error' => $e->getMessage()]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (empty($id)) {
            return $this->responErrorAjax("Xóa không thành công");
        }
        try {
            $this->categoryBlogRepository->softDelete($id);
            \request()->session()->flash('status', 'Xóa thành công!');
            return $this->responSuccessAjax(array(), "Xóa thành công");
        } catch (\Exception $e) {
            \request()->session()->flash('status', 'Xóa không thành công!');
            return $this->responErrorAjax("Xóa không thành công");
        }

    }

    public function responErrorAjax($message, $status = 400, $header = 'application/json')
    {
        $data = [
            'success' => false,
            'message' => $message
        ];
        return response($data, $status)
            ->header('Content-Type', $header);
    }

    public function responSuccessAjax($data=array(), $message, $status = 200, $header = 'application/json')
    {
        if (empty($data)){
            $data = [
                'success' => true,
                'message' => $message
            ];
        }
        return response($data, $status)
            ->header('Content-Type', $header);
    }
}
