<?php

namespace App\Http\Controllers;

use App\Repositories\MemberRepository;
use App\Repositories\PackageRepository;
use App\Repositories\SlideRepository;
use Illuminate\Http\Request;
use App\Repositories\PostRepository;

class HomeController extends Controller
{
    public $postRepository;
    public $slideRepository;
    public $memberRepository;
    public $packageRepository;

    public function __construct(PostRepository $postRepository, SlideRepository $slideRepository,
        MemberRepository $memberRepository, PackageRepository $packageRepository
    )
    {
        $this->postRepository = $postRepository;
        $this->slideRepository = $slideRepository;
        $this->memberRepository = $memberRepository;
        $this->packageRepository = $packageRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $specialVal = $this->postRepository->getPostByType(config('const.posts_category_key.specialVal'));
        $mission = $this->postRepository->getPostByType(config('const.posts_category_key.mission'));
        $banners = $this->slideRepository->getSlideByType(config('const.slide_category_key.banner'));
        $slideFeels = $this->slideRepository->getSlideByType(config('const.slide_category_key.feel'));
        $members = $this->memberRepository->getList();
        $packages = $this->packageRepository->getList();
        return view('home', compact('specialVal', 'mission', 'banners', 'slideFeels', 'members', 'packages'));
    }
}
