<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Model
{
    use SoftDeletes;


    protected $table = 'member';

    protected $fillable = [
        'name',
        'job',
        'avatar',
        'infor',
        'facebook_url',
        'insta_url',
        'sort',
        'user_id',
        'status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }
}
