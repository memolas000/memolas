<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackageOptionRelate extends Model
{
    use SoftDeletes;


    protected $table = 'package_option_relate';

    protected $fillable = [
        'option_id',
        'package_id',
        'price',
        'min_price',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }
}
