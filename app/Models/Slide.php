<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slide extends Model
{
    use SoftDeletes;


    protected $table ='slide';

    protected $fillable = [
        'title',
        'short_description',
        'image',
        'url',
        'sort',
        'title_seo',
        'img_seo',
        'content',
        'content_seo',
        'category_id',
        'facebook_url',
        'instal_url',
        'status',
        'user_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }
}
