<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use SoftDeletes;


    protected $table = 'package';

    protected $fillable = [
        'name',
        'avatar',
        'description',
        'price_fix',
        'max_price',
        'ratio_memolas',
        'discount',
        'status',
        'user_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }
}
