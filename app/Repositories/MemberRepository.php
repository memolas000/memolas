<?php

namespace App\Repositories;

use App\Models\Member;

/**
 * class MemberRepository.
 *
 * @package namespace App\Repositories;
 */
class MemberRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = Member::class;
    }

    public function totalClients()
    {
        return $this->getModel()->count();
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * get search condition
     */
    public function getList()
    {
        return $this->getModel()
            ->select('member.*', 'users.name as user_name')
            ->join('users', 'users.id', '=', 'member.user_id')
            ->orderBy('sort','ASC')
            ->get();
    }

    /**
     * @param $id
     * @return mixed
     * client detail get data
     */
    public function detail($id)
    {
        if (!is_numeric($id)) {
            return [];
        }
        return $this->getModel()
            ->select('member.*', 'users.name as user_name')
            ->join('users', 'users.id', '=', 'member.user_id')
            ->where('member.id', '=', $id)->first();
    }
}
