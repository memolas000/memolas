<?php

namespace App\Repositories;

use App\Models\CategoryBlog;
use App\Models\Option;
use App\Models\Slide;

/**
 * class CategoryBlogRepository.
 *
 * @package namespace App\Repositories;
 */
class CategoryBlogRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = CategoryBlog::class;
    }

    public function totalClients()
    {
        return $this->getModel()->count();
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * get search condition
     */
    public function getList()
    {
        return $this->getModel()
            ->select('category_blog.*', 'users.name as user_name')
            ->join('users', 'users.id', '=', 'category_blog.user_id')
            ->get();
    }

    /**
     * @param $id
     * @return mixed
     * client detail get data
     */
    public function detail($id)
    {
        if (!is_numeric($id)) {
            return [];
        }
        return $this->getModel()
            ->select('category_blog.*', 'users.name as user_name')
            ->join('users', 'users.id', '=', 'category_blog.user_id')
            ->where('category_blog.id', '=', $id)->first();
    }
}
