<?php

namespace App\Repositories;

use App\Models\Option;
use App\Models\PackageOptionRelate;
use App\Models\Slide;

/**
 * class PackageOptionRelateRepository.
 *
 * @package namespace App\Repositories;
 */
class PackageOptionRelateRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = PackageOptionRelate::class;
    }

    public function findByKey($packageId = 0, $optionId = 0)
    {
        if (empty($packageId) && empty($optionId)) {
            return [];
        }
        return $this->getModel()->select('id')
            ->when(!empty($packageId), function ($q) use ($packageId) {
                return $q->where('package_id', '=', $packageId);
            })
            ->when(!empty($optionId), function ($q) use ($optionId) {
                return $q->where('option_id', '=', $optionId);
            })
            ->first();
    }

}
