<?php

namespace App\Repositories;


use App\Models\Post;

/**
 * class PostsRepository.
 *
 * @package namespace App\Repositories;
 */
class PostRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = Post::class;
    }

    public function totalClients()
    {
        return $this->getModel()->count();
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * get search condition
     */
    public function getList()
    {
        return $this->getModel()
            ->select('posts.*', 'users.name as user_name')
            ->join('users', 'users.id', '=', 'posts.user_id')
            ->get();
    }

    /**
     * @param $id
     * @return mixed
     * client detail get data
     */
    public function detail($id)
    {
        if (!is_numeric($id)) {
            return [];
        }
        return $this->getModel()
            ->select('posts.*', 'users.name as user_name')
            ->join('users', 'users.id', '=', 'posts.user_id')
            ->where('posts.id', '=', $id)->first();
    }

    /**
     * @param $categoryId
     * @return mixed
     * get post by type
     */
    public function getPostByType($categoryId)
    {
        return $this->getModel()
            ->where('category_id', '=', $categoryId)
            ->where('status', '=', config('const.posts_status_key.enable'))
            ->first();
    }
    /**
     * @param $categoryId
     * @return mixed
     * get post by type
     */
    public function getListPostByType($categoryId)
    {
        return $this->getModel()
            ->where('category_id', '=', $categoryId)
            ->where('status', '=', config('const.posts_status_key.enable'))
            ->paginate(5);
    }

    /**
     * @param $id
     * @param $categoryId
     * @return bool
     *
     */
    public function updateAllStatus($id, $categoryId)
    {
        if (empty($categoryId) || empty($id)) {
            return false;
        }
        return $this->getModel()
            ->where('id', '!=', $id)
            ->where('category_id', '=', $categoryId)
            ->update(['status' => config('const.posts_status_key.disable')]);
    }
}
