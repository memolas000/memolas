<?php

namespace App\Repositories;

use App\Models\Option;
use App\Models\Slide;

/**
 * class OptionRepository.
 *
 * @package namespace App\Repositories;
 */
class OptionRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = Option::class;
    }

    public function totalClients()
    {
        return $this->getModel()->count();
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * get search condition
     */
    public function getList()
    {
        return $this->getModel()
            ->select('option.*', 'users.name as user_name')
            ->join('users', 'users.id', '=', 'option.user_id')
            ->get();
    }
    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * get search condition
     */
    public function getListOptionByCate($typeId)
    {
        return $this->getModel()
            ->select('option.*', 'users.name as user_name')
            ->join('users', 'users.id', '=', 'option.user_id')
            ->where('type', '=', $typeId)
            ->get();
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * get search condition
     */
    public function getPackageOption($packageId)
    {
        return $this->getModel()
            ->select('option.id as id','option.name', 'package_option_relate.price as option_price', 'package_option_relate.min_price as option_min_price')
            ->leftJoin('package_option_relate', 'package_option_relate.option_id', '=', 'option.id')
            ->where('package_option_relate.package_id', '=', $packageId)
            ->get()->keyBy('id');
    }
    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * get search condition
     */
    public function getAllPackageOption()
    {
        $data = $this->getModel()
            ->select('option.id as id',
                'package_option_relate.package_id as package_id',
                'package_option_relate.price as option_price',
                'package_option_relate.min_price as option_min_price'
            )
            ->leftJoin('package_option_relate', 'package_option_relate.option_id', '=', 'option.id')
            ->get();
        if (!empty($data)) {
            return $data->toArray();
        }
        return [];
    }

    /**
     * @param $id
     * @return mixed
     * client detail get data
     */
    public function detail($id)
    {
        if (!is_numeric($id)) {
            return [];
        }
        return $this->getModel()
            ->select('option.*', 'users.name as user_name')
            ->join('users', 'users.id', '=', 'option.user_id')
            ->where('option.id', '=', $id)->first();
    }
}
