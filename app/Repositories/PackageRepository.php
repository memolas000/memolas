<?php

namespace App\Repositories;

use App\Models\Package;

/**
 * class PackageRepository.
 *
 * @package namespace App\Repositories;
 */
class PackageRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = Package::class;
    }

    public function totalClients()
    {
        return $this->getModel()->count();
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * get search condition
     */
    public function getList()
    {
        return $this->getModel()
            ->select('package.*', 'users.name as user_name')
            ->join('users', 'users.id', '=', 'package.user_id')
            ->get();
    }


    /**
     * @param $id
     * @return mixed
     * client detail get data
     */
    public function detail($id)
    {
        if (!is_numeric($id)) {
            return [];
        }
        return $this->getModel()
            ->select('package.*', 'users.name as user_name')
            ->join('users', 'users.id', '=', 'package.user_id')
            ->where('package.id', '=', $id)->first();
    }
}
