<?php

namespace App\Repositories;

use App\Models\Slide;

/**
 * class SlideRepository.
 *
 * @package namespace App\Repositories;
 */
class SlideRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = Slide::class;
    }

    public function totalClients()
    {
        return $this->getModel()->count();
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * get search condition
     */
    public function getList()
    {
        return $this->getModel()
            ->select('slide.*', 'users.name as user_name')
            ->join('users', 'users.id', '=', 'slide.user_id')
            ->get();
    }

    /**
     * @param $id
     * @return mixed
     * client detail get data
     */
    public function detail($id)
    {
        if (!is_numeric($id)) {
            return [];
        }
        return $this->getModel()
            ->select('slide.*', 'users.name as user_name')
            ->join('users', 'users.id', '=', 'slide.user_id')
            ->where('slide.id', '=', $id)->first();
    }

    public function getSlideByType($categoryId)
    {
        return $this->getModel()
            ->where('category_id', '=', $categoryId)
            ->where('status', '=', config('const.posts_status_key.enable'))
            ->orderBy('sort','ASC')
            ->get();
    }
}
