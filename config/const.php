<?php

return [
    'status' => [
        'disable' => 0,
        'enable' => 1
    ],
    'posts_status' => [
        0 => 'Ẩn',
        1 => 'Hiện'
    ],
    'posts_status_key' => [
        'enable' => 1,
        'disable' => 0
    ],
    'slide_status' => [
        0 => 'Ẩn',
        1 => 'Hiện'
    ],
    'package_status' => [
        0 => 'Ẩn',
        1 => 'Hiện'
    ],
    'option_status' => [
        0 => 'Ẩn',
        1 => 'Hiện'
    ],
    'member_status' => [
        0 => 'Ẩn',
        1 => 'Hiện'
    ],
    'category_blog_status' => [
        0 => 'Ẩn',
        1 => 'Hiện'
    ],
    'posts_category' => [
        1 => 'Giá trị đặc biệt',
        2 => 'Sứ mệnh của memolas',
        3 => 'Trang giới thiệu',
        4 => 'Popup giới thiệu',
        5 => 'Blog',
    ],
    'posts_category_key' => [
        'specialVal' => 1,
        'mission' => 2,
        'introduce' => 3,
        'introduce_popup' => 4,
        'blog' => 5
    ],
    'option_type' => [
//        1 => 'Chiết khâu',
        1 => 'Chất liệu',
        2 => 'Option khác',
    ],
    'option_category_key' => [
//        1 => 'Chiết khâu',
        'material' => 1,
        'option_other' => 2,
    ],

    'slide_category' => [
        1 => 'Banner',
        2 => 'Cảm nhận học sinh',
        3 => 'Chủ đề yêu thích',
        4 => 'Popup giới thiệu',
    ],

    'slide_category_key' => [
        'banner' => 1,
        'feel' => 2,
        'subject_love' => 3,
        'popup_intro' => 4,
    ]
];