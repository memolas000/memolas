$.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

});
var a, b, c, maxPrice, packageId, optionId, nameOption, bia;
var t = N = total_student = totalPrice = rangePercent2 = oncePrice = 0;
var options = 1;

$(function () {
    packageId = $('#demoTabs .active').attr('data-package');
    optionId = $('input[name=material]:checked').val();
    getPriceDetail(packageId, optionId, rangePercent2);

    $('.nav-link').click(function () {
        packageId = $(this).attr('data-package');
        getPriceDetail(packageId, optionId, rangePercent2);
    });
    $('input[type=radio][name=material]').change(function () {
        optionId = $(this).val();
        getPriceDetail(packageId, optionId, rangePercent2);
    });

    $('.check_label').click(function () {
        var idOption = $(this).attr('for');
        nameOption = $('#' + idOption).attr('data-name');
        $('.bia').html(nameOption);
    });

    $('.range1').on('change input', function() {
        var rangePercent1 = $('.range1').val();
        $('.number1').text(rangePercent1);
        total_student = rangePercent1;
    });

    $('.range2').on('change input', function () {
        rangePercent2 = $('.range2').val();
        $('.number2').text(rangePercent2);
        getPriceDetail(packageId, optionId, rangePercent2);
    });


    $('#pass-json').click(function (e) {
        var person = {
            type: 4,
            numer_people: total_student,
            number_book: N,
            total_pages_book: 0,
            giam_gia: 0,
            option: options,
            bia: nameOption,
            tong: Math.ceil(totalPrice),
            name: $('#name').val(),
            email: $('#email').val(),
            number_phone: $('#sdt').val(),
            school: $('#school').val(),
            class: $('#class').val()
        };
        $.ajax({
            type: 'POST',
            url: 'api/mails/send-total',
            data: person,
            dataType: "json",
            success: function () {
                console.log('thành công');
            },
            error: function (error) {
                console.log(error);
            }
        })
    })
});

function getPriceDetail(packageId, optionId, rangePercent2) {
    N = parseFloat(rangePercent2);
    a = parseFloat($('#price_fix_' + packageId).val());
    b = parseFloat($('#option_package_price_' + optionId + '_' + packageId).val());
    c = parseFloat($('#ratio_memolas_' + packageId).val());
    minPrice = parseFloat($('#option_package_min_price_' + optionId + '_' + packageId).val());
    totalPrice = parseFloat(((((a + b) / (c/100)) * N * (1 - 0.003 * N) + t)));
    oncePrice = parseFloat(totalPrice/N);
    console.log(minPrice);
    console.log(oncePrice);
    if (oncePrice >= 0) {
        if (parseFloat(oncePrice) >= parseFloat(minPrice)) {
            $('.price-book').text(formatNumber(parseInt(oncePrice * 1000), '.', ',') + 'VND/Quyển');
        }else{
            $('.price-book').text(formatNumber(parseInt(minPrice * 1000), '.', ',') + 'VND/Quyển');
        }
    }
}
function formatNumber(nStr, decSeperate, groupSeperate) {
    nStr += '';
    x = nStr.split(decSeperate);
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + groupSeperate + '$2');
    }
    return x1 + x2;
}