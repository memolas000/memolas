function ChangeToSlug(string)
{
    var slug ='';
    //Đổi chữ hoa thành chữ thường
    var slug = string.toLowerCase();

    //Đổi ký tự có dấu thành không dấu
    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');
    //Xóa các ký tự đặt biệt
    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
    //Đổi khoảng trắng thành ký tự gạch ngang
    slug = slug.replace(/ /gi, "-");
    //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
    //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
    slug = slug.replace(/\-\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-/gi, '-');
    slug = slug.replace(/\-\-/gi, '-');
    //Xóa các ký tự gạch ngang ở đầu và cuối
    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');
    //In slug ra textbox có id “slug”
    return slug;
}

function modelSubmit(name, modal) {
    $('#' + modal).modal('hide');
    System.showLoading();
    $('#form_' + name).submit();
}
function showModal(id){
    $('#modalConfirmDelete_'+id).modal('show');
}
function deleteElement(id, url, title) {
    $.ajax({
        type: 'DELETE',
        url: url,
        data: {
            id: id
        },
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success: function (data) {
            if (data.success === true) {
                window.location.reload();
            } else {
            }
        },
        error: function (data) {
        }
    })
}

// function removeOption() {
//     $.ajax({
//         type: 'DELETE',
//         url: url,
//         data: {
//             id: id
//         },
//         headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
//         success: function (data) {
//             if (data.success === true) {
//
//             } else {
//             }
//         },
//         error: function (data) {
//         }
//     })
// }

$(document).ready(function () {
    $.noConflict();
    $('#posts-table').DataTable({
        // Config mặc định của dataTable
        responsive: true,
        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
        "oLanguage": {
            "oPaginate": {
                "sPrevious": "<",
                "sNext": ">"
            },
            "sInfo": "Hiển thị _START_ đến _END_ của _TOTAL_ bản ghi",
            "sInfoEmpty": "0 có kết quả",
            "sLengthMenu": "_MENU_",
            "sSearch": "",
        },
    });
    $('#slide-table').DataTable({
        // Config mặc định của dataTable
        responsive: true,
        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
        "oLanguage": {
            "oPaginate": {
                "sPrevious": "<",
                "sNext": ">"
            },
            "sInfo": "Hiển thị _START_ đến _END_ của _TOTAL_ bản ghi",
            "sInfoEmpty": "0 có kết quả",
            "sLengthMenu": "_MENU_",
            "sSearch": "",
        },
    });
    $('#package-table').DataTable({
        // Config mặc định của dataTable
        responsive: true,
        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
        "oLanguage": {
            "oPaginate": {
                "sPrevious": "<",
                "sNext": ">"
            },
            "sInfo": "Hiển thị _START_ đến _END_ của _TOTAL_ bản ghi",
            "sInfoEmpty": "0 có kết quả",
            "sLengthMenu": "_MENU_",
            "sSearch": "",
        },
    });
    $('#option-table').DataTable({
        // Config mặc định của dataTable
        responsive: true,
        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
        "oLanguage": {
            "oPaginate": {
                "sPrevious": "<",
                "sNext": ">"
            },
            "sInfo": "Hiển thị _START_ đến _END_ của _TOTAL_ bản ghi",
            "sInfoEmpty": "0 có kết quả",
            "sLengthMenu": "_MENU_",
            "sSearch": "",
        },
    });
    $('#member-table').DataTable({
        // Config mặc định của dataTable
        responsive: true,
        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
        "oLanguage": {
            "oPaginate": {
                "sPrevious": "<",
                "sNext": ">"
            },
            "sInfo": "Hiển thị _START_ đến _END_ của _TOTAL_ bản ghi",
            "sInfoEmpty": "0 có kết quả",
            "sLengthMenu": "_MENU_",
            "sSearch": "",
        },
    });

    $('#menuToggle').click(function(){
        if ($('body').hasClass('open')){
            $('#right-panel').css('margin-left', '260px');
        }else{
            $('#right-panel').css('margin-left', '100px');
        }
    });

    CKEDITOR.editorConfig = function( config ) {
        config.language = 'es';
        config.uiColor = '#F7B42C';
        config.height = 300;
        config.toolbarCanCollapse = true;
    };
});

