@extends('frontend.layouts.master')

@section('content')
<section class="banner">
    <div class="bannerSlider container">
        @if(!empty($banners))
            @foreach($banners as $banner)
                <div class="slider row">
                    <div class="item col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
                        <div class="description-banner">
                            <p>{!!  !empty($banner->content) ? html_entity_decode($banner->content) : '' !!}</p>
                            <a href="">Đăng kí</a>
                        </div>
                    </div>
                    <img class="img-banner" onclick="redirectBanner(this);" banner-url = "{{$banner->url}}"
                         src="{{ secure_asset(!empty($banner->image) ? '/upload/slide/'.$banner->image : 'images/default.png') }}"
                            alt="{{$banner->img_seo}}">
                    <div class="social">
                        <a href="{{$banner->facebook_url}}" target="_blank"><img src="{{secure_asset('images/fbo.png')}}" alt=""></a>
                        <a href="{{$banner->instal_url}}" target="_blank"><img src="{{secure_asset('images/intaso.png')}}" alt=""></a>
                    </div>
                </div>

            @endforeach
        @endif
    </div>
</section>

<section class="container fit">
    <div class="header wow fadeInUp">
        <h1>
            Niên yếu phù hợp với ai?
        </h1>
        <p>Nếu bạn là thành viên của 1 tập thể thì Niên yếu dành cho bạn và tập thể đó</p>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 box wow fadeInLeft" data-wow-delay="0s">
                <span>Nhà Trường</span>
                <img src="{{ secure_asset('images/schools.png') }}" alt="">
            </div>
            <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 box wow fadeInLeft " data-wow-delay="0.2s">
                <span>Phụ Huynh</span>
                <img src="{{ secure_asset('images/ph.png') }}" alt="">
            </div>
            <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 box wow fadeInLeft" data-wow-delay="0.4s">
                <span>Học Sinh</span>
                <img src="{{ secure_asset('images/student.png') }}" alt="">
            </div>
        </div>
    </div>
</section>
@if(!empty($specialVal))
    <section class="container characteristics">
        <div class="header wow fadeInUp">
            <h1>
                {{$specialVal->title}}
            </h1>
            <p>{{$specialVal->short_description}}</p>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-12 col-sm-5 col-md-5 col-xl-5 col-lg-5 text wow fadeInLeft">
                    <div class="">
                        <p>
                            {!! html_entity_decode($specialVal->content)!!}
                        </p>
                        <a href="{{route('about')}}">Tìm Hiểu Thêm</a>
                    </div>
                </div>
                <div class="col-12 col-sm-7 col-md-7 col-xl-7 col-lg-7 image wow fadeInRight">
                    <img src="{{ secure_asset('/upload/posts/'. $specialVal->avatar_post) }}" alt="{{$specialVal->img_seo}}">
                </div>
            </div>
        </div>
    </section>
@endif
<section class="container step">
    <div class="header" wow fadeInUp>
        <h1>
            Các Bước Thực Hiện Niên Yếu
        </h1>
    </div>
    <div class="content">
        <div class="row box">
            <div class="item wow zoomInLeft" data-wow-delay="0s">
                <div class="title">
                    <img src="{{ secure_asset('images/table1.png') }}" alt="">
                    <div>
                        <h5>Bước 2</h5>
                        <img src="{{ secure_asset('images/icontable1.png') }}" alt="">
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paragraph">
                    <p>Cùng lớp mình trải qua và tận hưởng năm học đầy thú vị này</p>
                </div>
            </div>
            <div class="item wow zoomInLeft" data-wow-delay="0.4s">
                <div class="title">
                    <img src="{{ secure_asset('images/table2.png') }}" alt="">
                    <div>
                        <h5>Bước 3</h5>
                        <img src="{{ secure_asset('images/icontable2.png') }}" alt="">
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paragraph">
                    <p>Cùng nhau chia sẻ những suy nghĩ, những cảm xúc buồn đang xảy ra. </p>
                </div>
            </div>
            <div class="item wow zoomInLeft" data-wow-delay="0.8s">
                <div class="title">
                    <img src="{{ secure_asset('images/table3.png') }}" alt="">
                    <div>
                        <h5>Bước 4</h5>
                        <img src="{{ secure_asset('images/icontable3.png') }}" alt="">
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paragraph">
                    <p>Cùng nhau cười thật nhiều</p>
                </div>
            </div>
            <div class="item wow zoomInLeft" data-wow-delay="1.2s">
                <div class="title">
                    <img src="{{ secure_asset('images/table4.png') }}" alt="">
                    <div>
                        <h5>Bước 5</h5>
                        <img src="{{ secure_asset('images/icontable4.png') }}" alt="">
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paragraph">
                    <p>Cùng nhau lưu giữ lại</p>
                </div>
            </div>
            <div class="item wow flipInX" data-wow-delay="1.6s">
                <div class="title">
                    <img src="{{ secure_asset('images/table5.png') }}" alt="">
                    <div>
                        <h5>Bước 1</h5>
                        <img src="{{ secure_asset('images/icontable5.png') }}" alt="">
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paragraph">
                    <p>Liên hệ với Memolas để lên ý tưởng cho lớp</p>
                </div>
            </div>
        </div>
    </div>
</section>
@if(!empty($slideFeels))
    <section class="feedback">
        <div class="container">
            <h1 style="text-align: center;" class="header wow fadeInUp">Cảm nhận của các bạn học sinh</h1>
            <div class="feed">
                @foreach($slideFeels as $slide)
                    <div class="user col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
                        <div class="b col-12 col-sm-12 col-md-12 col-lg-10 col-xl-10 offset-lg-1 offset-xl-1">
                            <div class="avata">
                                <img src="{{!empty($slide->image) ? secure_asset('upload/slide/'.$slide->image)
                                 : secure_asset('images/default.png') }}" alt="{{$slide->img_seo}}">
                            </div>
                            <div class="content">
                                <h3>
                                    {{$slide->title}}
                                </h3>
                                <span>
                                    {{$slide->short_description}}
                        </span>
                                <p class="col-12 col-sm-12 col-md-10 col-xl-10 col-lg-10 offset-md-1 offset-xl-1 offset-lg-1">
                                    {!! html_entity_decode($slide->content) !!}
                                </p>
                            </div>

                            <div class="social-person">
                                @if(!empty($slide->facebook_url))
                                    <a href="{{$slide->facebook_url}}"><img
                                            src="{{ secure_asset('images/fbo.png') }}" alt=""></a>
                                @endif
                                @if(!empty($slide->instal_url))
                                        <a href="{{$slide->instal_url}}"><img
                                                    src="{{ secure_asset('images/intaso.png') }}" alt=""></a>
                                    @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endif
@if(!empty($mission))
    <section class="memolas container">
        <h1 class="header wow fadeInUp">{{$mission->title}}</h1>
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-lg-6 image wow fadeInLeft">
                <img width="100%" src="{{ secure_asset('/upload/posts/'.$mission->avatar_post) }}" alt="{{$mission->img_seo}}">
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-lg-6 content wow fadeInRight">
                <h3>{{$mission->short_description}}</h3>
                <p>
                    {!! html_entity_decode($mission->content) !!}
                </p>
                <a id="modal-team" href="#" data-izimodal-open="#team" data-izimodal-transitionin="fadeInDown">Tìm hiểu
                    thêm</a>
            </div>
            <div class="team" id="team">
                @if(!empty($members))
                    <div>
                        <div class="box listteam" id="team-company">
                            @foreach($members as $member)
                                <div class="item col-12 col-sm-12 col-md-3 col-lg-3 col-lg-3">
                                    <div class="boxchild">
                                        <div class="header">
                                            <img src="{{!empty($member->avatar) ? secure_asset('upload/member/'.$member->avatar)
                                        :secure_asset('images/avataitem.png')}}" alt="">
                                        </div>
                                        <div class="content-item">
                                            <span>{{$member->job}}</span>
                                            <h3>{{$member->name}}</h3>
                                            <hr>
                                            <div>
                                                <a href="{{$member->facebook_url}}" target="_blank"><img
                                                            src="{{secure_asset('images/fb1.png')}}" alt=""></a>

                                                <a href="{{$member->instal_url}}"
                                                   target="_blank"><img src="{{secure_asset('images/in.png')}}" alt=""></a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach;
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endif
@if(!empty($packages))
    <section class="options container">
        <div class="header wow fadeInUp">
            <h1>
                Các gói thực hiện Niên yếu
            </h1>
            <p>Chọn số quyển để memolas tính toán chi phí cho bạn</p>

        </div>
        <div class="row content">
            @foreach($packages as $package)
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 wow zoomIn" data-wow-delay="0.4s">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 price">
                        <div class="header">
                            <img src="{{ !empty($package->avatar) ?  secure_asset('upload/package/'.$package->avatar)
                            : secure_asset('images/default.png') }}" alt="">
                            <h4>{{$package->name}}</h4>
                        </div>
                        {!! html_entity_decode($package->description) !!}
                        <div class="CTA">
                            <h5>Chi phí: <span id="price-1">0 VND</span>/Quyển</h5>
                            <a class="col-12 col-sm-12" href="{{route('total')}}">Tính toán chi phí</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="header wow fadeInUp" style=" margin-top: 30px;">
            <div class="range col-12 col-sm-12">

                <input class="quyen" type="range" min="0" max="300" step="1" value="0">
                <div class="info">
                    <div class="t1">Chọn số quyển dự kiến</div>
                    <div class="text">
                        <span class="value">0</span>
                        <span class="material-icons">menu_book</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif
<section class="we">
    <h1 class="wow fadeInUp">Đăng ký đồng hành cùng chúng tôi</h1>

    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-lg-6 content">
                <h3>Bạn là</h3>
                <ul class="nav nav-tabs">
                    <li class="nav-item wow fadeInUp">
                        <a id="hs" class="nav-link active" data-toggle="tab" href="#home">Học Sinh</a>
                    </li>
                    <li class="nav-item wow fadeInUp">
                        <a id="ph" class="nav-link" data-toggle="tab" href="#menu1">Phụ Huynh</a>
                    </li>
                    <li class="nav-item wow fadeInUp">
                        <a id="gv" class="nav-link" data-toggle="tab" href="#menu2">Giáo viên</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane container row active" id="home">
                        <form id="send-email1" action="" method="">
                            <input id="name-hs" class="wow fadeInLeft" type="text" placeholder="Họ và tên bạn" data-wow-delay="0s">
                            <div class="ngang">
                                <input id="classroom-hs" class="wow fadeInLeft" type="text" placeholder="Lớp" data-wow-delay="0.2s">
                                <input id="CLB-hs" class="wow fadeInLeft" type="text" placeholder="CLB" data-wow-delay="0.4s">
                            </div>
                            <input id="school-hs" class="wow fadeInLeft" type="text" placeholder="Trường" data-wow-delay="0.6s">
                            <input id="email-hs" class="wow fadeInLeft" type="text" placeholder="Email" data-wow-delay="0.8s">
                            <input id="sdt-hs" class="wow fadeInLeft" type="text" placeholder="Số điện thoại" data-wow-delay="1s">
                            <input id="number-hs" class="wow fadeInLeft" type="number" placeholder="Số Quyển" data-wow-delay="1.2s">
                            <input id="submit" class="wow flipInX submit" type="submit" value="Gửi" >
                            <a href="" id="" class="wow flipInX submitDk">Đăng kí ngay</a>
                        </form>
                    </div>
                    <div class="tab-pane container fade row" id="menu1">
                        <input id="name-ph" type="text" placeholder="Họ tên bạn">
                        <input id="name-child-ph" type="text" placeholder="Họ tên con">
                        <div class="ngang">
                            <input id="class-ph" type="text" placeholder="Lớp">
                            <input id="school-ph" type="text" placeholder="Trường">
                        </div>

                        <input id="email-ph" type="text" placeholder="Email liên hệ">
                        <input id="sdt-ph" type="text" placeholder="Số điện thoại của bạn">
                        <input id="number-ph" type="number" placeholder="Số quyển dự kiến">
                        <input id="submit1" class="wow flipInX submit" type="submit" value="Gửi">
                        <a href="" id="" class="wow flipInX submitDk">Đăng kí ngay</a>
                    </div>
                    <div class="tab-pane container fade row" id="menu2">
                        <input id="name-gv" type="text" placeholder="Họ tên bạn">
                        <input id="subject-gv" type="text" placeholder="Bạn là giáo viên môn">
                        <div class="ngang">
                            <input id="class-gv" type="text" placeholder="Lớp">
                            <input id="school-gv" type="text" placeholder="Trường">
                        </div>

                        <input id="email-gv" type="text" placeholder="Email liên hệ">
                        <input id="sdt-gv" type="text" placeholder="Số điện thoại của bạn">
                        <input id="number-gv" type="number" placeholder="Số quyển dự kiến">
                        <input id="submit2" class="wow flipInX submit" type="submit" value="Gửi">
                        <a href="" id="" class="wow flipInX submitDk">Đăng kí ngay</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script src="{{ secure_asset('js/frontend/wow.js')}}"></script>
<script src="{{ secure_asset('js/frontend/slick.js')}}"></script>
<script src="{{ secure_asset('js/frontend/slick.min.js')}}"></script>
<script src="{{ secure_asset('js/frontend/event.js')}}"></script>
<!-- <script src="{{ secure_asset('js/frontend/range.js')}}"></script> -->
<script type="text/javascript" src="{{asset('js/frontend/iziModal.min.js')}}"></script>
<script src="{{ secure_asset('js/frontend/send-email.js')}}"></script>
<script>
    $(document).ready(function() {
        $(".team").iziModal({
                title: "Danh sách những thành viên trong team",
                subtitle: "",
                headerColor: "#70C3FF",
                width: 1170,
                fullscreen: true,
                openFullscreen: false,
                autoOpen: false,
                onOpening: function() {
                    $('.listteam').slick({
                        dots: true,
                        infinite: true,
                        speed: 300,
                        slidesToShow: 4,
                        slidesToScroll: 2,
                        autoplay:true,
                        autoplaySpeed: 2000,
                        responsive: [
                            {
                                breakpoint: 1024,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 1,
                                    infinite: true,
                                    dots: true
                                }
                            }
                        ]
                    });
                }
            });
        
    })
    function redirectBanner(e) {
        location.href = $(e).attr('banner-url');
    }
</script>
@endsection
    