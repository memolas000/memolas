@extends('frontend.layouts.master')

{{--@section('css')--}}
{{--    <link rel="stylesheet" type="text/css" href="{{asset('./css/frontend/total.css')}}">--}}
{{--@endsection--}}

@section('content')
    <section style="margin-top: 30px;" class="container price">
        <div class="header">
            <h3>Tính toán chi phí thực hiện Niên yếu</h3>
            <ul class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 offset-lg-2 offset-xl-2">
                <li>Giá thành được tính trên 1 quyển</li>
                <li>Giá đã bao gồm phí vận chuyển </li>
                <li>Đây chỉ là giá ước lượng theo cách tính của MemoLas. Bạn luôn có thể thay đổi số trang, chất
                    liệu,v..v.. trong quá trình thực hiện</li>
            </ul>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                    <div>
                        <input class="range1" type="range" value="0">
                        <div class="text">
                            <span>Số học sinh</span>
                            <span class="number1">0</span>
                        </div>
                    </div>
                    <div>
                        <input class="range2" type="range" value="0">
                        <div class="text">
                            <span>Số quyển dự kiến</span>
                            <div>
                                <span class="">( Thêm 1 quyển tặng giáo viên nhé )</span>
                                <span class="number2">0</span>
                            </div>
                        </div>
                    </div>
                    <div id="demoTabs">
                        @if (!empty($package))
                            <ul class="nav nav-tabs option">
                                @foreach($package as $key => $value)
                                    <li class="nav-item wow fadeInUp">
                                        <a class="nav-link {{($key == 0 ? 'active' : '')}} b1" data-toggle="tab" data-package ="{{$value['id']}}" href="#menu_package_{{$value['id']}}"> {{$value['name']}}</a>
                                        <input type="hidden" value="{{$value['id']}}" name="package_id[]" id="package_id_{{$value['id']}}">
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>

                    <!-- Tab panes -->
                    @if (!empty($package))
                        <div class="tab-content" id="package_info">
                            @foreach($package as $key => $value)
                                <div class="tab-pane container row {{($key == 0) ? 'active' : ''}}" id="menu_package_{{$value['id']}}">
                                    <ul class="list">
                                        {!! html_entity_decode($value['description']) !!}
                                    </ul>
                                    <input type="hidden" value="{{$value['price_fix']}}" name="price_fix[]" id="price_fix_{{$value['id']}}">
                                    <input type="hidden" value="{{$value['ratio_memolas']}}" name="ratio_memolas[]" id="ratio_memolas_{{$value['id']}}">
                                </div>
                            @endforeach
                        </div>
                    @endif
                    <h3>Thông tin chi tiết báo giá</h3>
                    <p>Điền các thông tin bên dưới giúp mình ha</p>
                    @if (!empty($option))
                        <div class="check-group">
                            @foreach($option as $key => $value)
                                <div class="radiobtn" style="margin-right: 30px; float: left;">
                                    <input class="radio" type="radio" id="material_{{$value['id']}}"
                                           name="material" value="{{$value['id']}}"
                                           data-name = "{{$value['name']}}"
                                           {{($key == 0) ? 'checked' : ''}}/>
                                    <label id='checked_{{$value['id']}}' class="check_label" for="material_{{$value['id']}}">{{$value['name']}}</label>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
                @if (!empty($optionPackage))
                    @foreach($optionPackage as $key => $value)
                        <input type="hidden" id="option_package_price_{{$value['id']}}_{{$value['package_id']}}" value="{{$value['option_price']}}">
                        <input type="hidden" id="option_package_min_price_{{$value['id']}}_{{$value['package_id']}}" value="{{$value['option_min_price']}}">
                    @endforeach
                @endif
                <div class="col-6 col-sm-6 col-md-6 col-lg-4 col-xl-4 offset-lg-2 offset-xl-2 total">
                    <h3>Báo giá chi tiết</h3>
                    @if (!empty($option))
                    <div class="info">
                        @foreach($option as $key => $value)
                            @if($key == 0)
                                <span></span>
                                <span class="bia">{{$value['name']}}</span>
                            @endif
                        @endforeach
                    </div>
                    @endif
                    <hr>
                    <div class="info">
                        <span>Giá thành</span>
                        <span class="price-book">0VND/Quyển</span>
                    </div>
                    <!-- <div class="info">
                        <span>Tổng chi phí</span>
                        <span id="total">300.000</span>
                    </div> -->
                    <input class="box" type="text" id="name" placeholder="Họ và tên">
                    <input class="box" type="email" id="email" placeholder="Email của bạn">
                    <input class="box" type="number" id="sdt" placeholder="Số điện thoại của bạn" >
                    <input class="box" type="text" id="school" placeholder="Trường">
                    <input class="box" type="text" id="class" placeholder="lớp">
                    <input type="submit" id="pass-json" value="Gửi yêu cầu">
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
<script src="{{ secure_asset('js/frontend/wow.js')}}"></script>
<script src="{{ secure_asset('js/frontend/slick.js')}}"></script>
<script src="{{ secure_asset('js/frontend/slick.min.js')}}"></script>
<script src="{{ secure_asset('js/frontend/event.js')}}"></script>
<script src="{{ secure_asset('js/frontend/send-email.js')}}"></script>
<script src="{{ secure_asset('js/frontend/range_custom.js')}}"></script>
@endsection
