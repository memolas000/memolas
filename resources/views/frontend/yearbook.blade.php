@extends('frontend.layouts.master')

@section('css')
    <link rel="stylesheet" href="{{ secure_asset('css/frontend/aboutus.css') }}">
@endsection

@section('content')
    @if($introduce)
        <section class="banner-about">
            <img class="img-pc" width="100%" height="100%" src="{{secure_asset('images/aboutus.png')}}" alt="">
            <img class="top wow fadeIn" width="100%" height="100%" src="{{secure_asset('images/aboutus1.png')}}" alt="">
            <div class="content col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                <h3>{{$introduce->title}}</h3>
                <p>
                   {!! html_entity_decode($introduce->content) !!}
                </p>
                <a class="icon" href="#" data-izimodal-open="#abouts" data-izimodal-transitionin="fadeInDown"><i
                            class="material-icons iconright-mobile">arrow_right_alt</i></a>
            </div>
        </section>
    @endif
    <div class="abouts" id="abouts">
        <div class="box container">
            <div class="item col-12 col-sm-12 col-md-12 col-lg-12 col-lg-12">
                @if (!empty($introducePopup))
                    <br/>
                    <p>{!! !empty($introducePopup->content) ? html_entity_decode($introducePopup->content) : ''!!}</p>
                @endif
            </div>
            @if(!empty($slideIntroduce))
                <div class="list">
                    @foreach($slideIntroduce as $slider)
                        <img src="{{ !empty($slider->image) ? secure_asset('upload/slide/'.$slider->image)
                    : secure_asset('image/default.png')}}" alt="">
                    @endforeach
                </div>
            @endif
            <p>
            <p>{!! !empty($introducePopup->content_2) ? html_entity_decode($introducePopup->content_2) : ''!!}</p>


        </div>
    </div>
    <section class="about-book">
        <div class="container content">
            <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5 offset-lg-7 offset-xl-7 wow fadeInRight">
                <div class='box-text'>
                    <h3>Bí kíp thực hiện Niên yếu</h3>
                    <p>Niên yếu được thực hiện bởi rất nhiều công đoạn, rất nhiều con người, và rất nhiều những
                        deadline. Và đó cũng mới chỉ vừa đủ. Để Niên yếu của lớp trở nên hay ho thú vị với những chi
                        tiết độc đáo thì chúng ta cần thủ sẵn một vài mẹo nhỏ. Đây là bí kíp được đúc kết qua những năm
                        tháng kinh nghiệm của MemoLas. Hy vọng cuốn bí kíp này sẽ giúp bạn có cái nhìn tổng quan hơn của
                        một người thực hiện Niên yếu.</p>
                    <div class="btn-action">
                        <div class="btn-link"><a href="http://design.memolas.vn">Sử dụng ứng dụng</a></div>
                        <div class="btn-link"><a href="{{secure_asset('file/document.pdf')}}"
                                                 download="document (ver. 2.0.1).pdf">Download bí kíp</a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if(!empty($slideSubjectLoves))
        <section class="container template">
            <h3 class="wow fadeInDown">Các mẫu thiết kế Niên yếu</h3>
            <div class="slider-aboutus">
                <div class="slider-template wow fadeInUp">
                    @foreach($slideSubjectLoves as $slide)
                        <div class="item">
                            <img src="{{!empty($slide->image) ?  secure_asset('upload/slide/'.$slide->image)
                            : secure_asset('images/default.png')}}" alt="">
                            <div class="text">
                                <span>{{$slide->title}}</span>
                                <h4>{{$slide->short_description}}</h4>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif
@endsection

@section('js')
    <script src="{{ secure_asset('js/frontend/wow.js')}}"></script>
    <script src="{{ secure_asset('js/frontend/slick.js')}}"></script>
    <script src="{{ secure_asset('js/frontend/slick.min.js')}}"></script>
    <script src="{{ secure_asset('js/frontend/event.js')}}"></script>

    <script type="text/javascript" src="{{secure_asset('js/frontend/iziModal.min.js')}}"></script>
    <script>
        $(function () {
            $('.abouts').iziModal({
                title: '{{!empty($introducePopup->title) ? $introducePopup->title : ''}}',
                subtitle: '',
                headerColor: '#70C3FF',
                width: 1170,
                fullscreen: true,
                openFullscreen: false,
                autoOpen: false,
                navigateArrows: true,
            });
        });
        $(document).ready(function () {
            $('.list').slick({
                dots: false,
                infinite: true,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 2,
                autoplay: true,
                autoplaySpeed: 4000,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            infinite: true,
                            dots: true
                        }
                    }
                ]
            });
        })
    </script>
@endsection
