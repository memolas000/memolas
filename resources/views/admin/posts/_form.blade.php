@extends('admin.layouts.master')

@section('content')
    <section class="breadcrumbs">
        @if(session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
    </section>
    <div class="header mt-md-5">
        <div class="page-breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="/">Trang chủ</a>
                </li>
                <li><a href="{{route('admin.posts.index')}}">Danh sách bài viết</a></li>
                <li class="active">{{!empty($id) ? 'Chỉnh sửa bài post' : 'Tạo mới'}}</li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <h1 class="text-left mb-5 title">{{!empty($id) ? 'Chỉnh sửa bài post số '.$id : 'Tạo mới bài post'}}</h1>
        <div class="row">
            <form id="posts-add-form" method="post" class="form-horizontal"
                  action="{{!empty($id) ? route("admin.posts.update",[$id]) : route("admin.posts.store")}}">
                {{ csrf_field() }}
                {{ !empty($id) ? method_field('PATCH') :'' }}
                <div class="form-group">
                    <label class="col-lg-2 control-label">Tiêu đề<sup>*</sup></label>
                    <div class="col-lg-10 posts-element">
                        <input type="text" class="form-control" name="title" id="posts_title"
                               value="{{oldData($data,'title','')}}"
                               placeholder="Nhập tiêu đề"/>
                        @include('admin.elements.validate_error', ['message' => $errors->first('title')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Tiêu đề seo<sup>*</sup></label>
                    <div class="col-lg-10 posts-element">
                        <input type="text" class="form-control" name="title_seo" id="title_seo" readonly
                               value="{{oldData($data,'title_seo','')}}"
                               placeholder="Tiêu đề seo"/>
                        @include('admin.elements.validate_error', ['message' => $errors->first('title_seo')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Mô tả ngắn</label>
                    <div class="col-lg-10 posts-element">
                        <textarea rows="5" class="form-control" name="short_description" id="short_description">{{oldData($data,'short_description','')}}
                        </textarea>
                        @include('admin.elements.validate_error', ['message' => $errors->first('short_description')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Nội dung<sup>*</sup></label>
                    <div class="col-lg-10 posts-element">
                        <textarea class="form-control" name="content" id="posts_content">{!!  oldData($data,'content','')!!}
                        </textarea>
                        @include('admin.elements.validate_error', ['message' => $errors->first('content')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Nội dung 2</label>
                    <div class="col-lg-10 posts-element">
                        <textarea class="form-control" name="content_2" id="posts_content_2">{!!  oldData($data,'content_2','')!!}
                        </textarea>
                        @include('admin.elements.validate_error', ['message' => $errors->first('content_2')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Vị trí hiển thị<sup>*</sup></label>
                    <div class="col-lg-5 user-element">
                        <select style="width:100%;" name="category_id" class="form-control">
                            @php($categoryId = oldData($data, 'category_id', 0))
                            <option value="">Chọn vị trí</option>
                            @foreach($category as $key => $value)
                                <option {{($categoryId == $key) ? 'selected' : ''}} value="{{$key}}"/>{{$value}}
                            @endforeach
                        </select>
                        @include('admin.elements.validate_error', ['message' => $errors->first('category_id')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Chọn loại blog</label>
                    <div class="col-lg-5 user-element">
                        <select style="width:100%;" name="category_blog" class="form-control">
                            @php($categoryBlogId = oldData($data, 'category_blog', 0))
                            <option value="">Chọn loại blog</option>
                            @foreach($categoryBlog as $key => $value)
                                <option {{($categoryBlogId == $value['id']) ? 'selected' : ''}} value="{{$value['id']}}"/>{{$value['name']}}
                            @endforeach
                        </select>
                        @include('admin.elements.validate_error', ['message' => $errors->first('category_blog')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Trạng thái<sup>*</sup></label>
                    <div class="col-lg-5 user-element">
                        <select style="width:100%;" name="status" class="form-control form-status">
                            @php($status = oldData($data, 'status', 0))
                            @foreach($postsStatus as $key => $value)
                                <option {{($status == $key) ? 'selected' : ''}} value="{{$key}}"/>{{$value}}
                            @endforeach
                        </select>
                        @include('admin.elements.validate_error', ['message' => $errors->first('status')])
                    </div>
                </div>
                <div class="form-group upload-posts">
                    <label class="col-lg-2 control-label">Ảnh đại diện</label>
                    <div class="col-lg-10 no_pad_left">
                        <div class="col-lg-4 img-posts">
                            @php($avatar = oldData($data, 'avatar_post',''))
                            @if(empty($avatar))
                                <img src="{{asset('/images/upload-default.png')}}" name="image_url"
                                     id="image_url">
                            @else
                                <img src="{{asset('/upload/posts/'.$avatar)}}" name="image_url"
                                     id="image_url">
                            @endif
                            @include('admin.elements.validate_error', ['message' => $errors->first('post_file')])
                        </div>
                        <div class="col-lg-8 notification-element">
                            <label for="post_file" class="btn btn-primary">Chọn ảnh</label>
                            <input type="file" id="post_file" name="post_file" class="upload-input"
                                   style="visibility:hidden;"/>
                            <input type="hidden" name="avatar_post" id="avatar_post" class="upload-input" value="{{$avatar}}"
                                   style="visibility:hidden;"/>

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">ALT Seo<sup></sup></label>
                    <div class="col-lg-10 posts-element">
                        <input type="text" class="form-control" name="img_seo" id="posts_img_seo"
                               value="{{oldData($data,'img_seo','')}}"
                               placeholder="Nhập alt seo ảnh"/>
                        @include('admin.elements.validate_error', ['message' => $errors->first('img_seo')])
                    </div>
                </div>

                <div class="form-group row" style="margin-top: 40px;">
                    <div class="col-sm-2 col-form-label"></div>
                    <div class="col-sm-10 button-submit">
                        <button class="btn btn-warning btn-reset" type="button" onclick="location.reload();">
                            Nhập lại
                        </button>
                        <button class="btn btn-success submit-posts" type="submit">
                            Lưu
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection
@php($urlUpload = !empty($id) ? '../uploadFile' : '../posts/uploadFile')
@section('js')
    <script>
        CKEDITOR.replace('posts_content');
        CKEDITOR.replace('posts_content_2');
        $('input[type="file"]').change(function () {
            var formData = new FormData();
            var fileToUpload = $('#post_file').prop('files')[0];
            formData.append("fileToUpload", fileToUpload);
            $.ajax({
                url: '{{$urlUpload}}',
                method: 'POST',
                data: formData,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                processData: false,
                contentType: false,
                cache: false,
                success: function (result) {
                    $('.img-posts img').attr("src", "{{asset('upload/posts')}}/" + result);
                    $('#avatar_post').val(result);
                }
            });
        });
        $('#posts_title').keyup(function () {
            var titleSeo = ChangeToSlug($(this).val());
            $('#title_seo').val(titleSeo);
        })
    </script>
@endsection