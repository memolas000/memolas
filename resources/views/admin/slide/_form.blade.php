@extends('admin.layouts.master')

@section('content')
    <section class="breadcrumbs">
        @if(session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
    </section>
    <div class="header mt-md-5">
        <div class="page-breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="/">Trang chủ</a>
                </li>
                <li><a href="{{route('admin.slide.index')}}">Danh sách bài slide</a></li>
                <li class="active">{{!empty($id) ? 'Chỉnh sửa slide' : 'Tạo mới'}}</li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <h1 class="text-left mb-5 title">{{!empty($id) ? 'Chỉnh sửa slide số '.$id : 'Tạo mới slide'}}</h1>
        <div class="row">
            <form id="slide-add-form" method="post" class="form-horizontal"
                  action="{{!empty($id) ? route("admin.slide.update",[$id]) : route("admin.slide.store")}}">
                {{ csrf_field() }}
                {{ !empty($id) ? method_field('PATCH') :'' }}
                <div class="form-group">
                    <label class="col-lg-2 control-label">Tiêu đề</label>
                    <div class="col-lg-10 slide-element">
                        <input type="text" class="form-control" name="title" id="slide_title"
                               value="{{oldData($data,'title','')}}"
                               placeholder="Nhập tiêu đề"/>
                        @include('admin.elements.validate_error', ['message' => $errors->first('title')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Tiêu đề seo</label>
                    <div class="col-lg-10 slide-element">
                        <input type="text" class="form-control" name="title_seo" id="slide_title_seo" readonly
                               value="{{oldData($data,'title_seo','')}}"
                               placeholder="Tiêu đề seo"/>
                        @include('admin.elements.validate_error', ['message' => $errors->first('title_seo')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Link url</label>
                    <div class="col-lg-10 slide-element">
                        <input type="text" class="form-control" name="url" id="slide_url"
                               value="{{oldData($data,'url','')}}"
                               placeholder="Nhập link url"/>
                        @include('admin.elements.validate_error', ['message' => $errors->first('url')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Link facebook</label>
                    <div class="col-lg-10 slide-element">
                        <input type="text" class="form-control" name="facebook_url" id="slide_facebook_url"
                               value="{{oldData($data,'facebook_url','')}}"
                               placeholder="Nhập link facebook"/>
                        @include('admin.elements.validate_error', ['message' => $errors->first('facebook_url')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Link instal</label>
                    <div class="col-lg-10 slide-element">
                        <input type="text" class="form-control" name="instal_url" id="slide_instal_url"
                               value="{{oldData($data,'instal_url','')}}"
                               placeholder="Nhập link instal"/>
                        @include('admin.elements.validate_error', ['message' => $errors->first('instal_url')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Mô tả ngắn</label>
                    <div class="col-lg-10 slide-element">
                        <textarea rows="5" class="form-control" name="short_description" id="short_description">{{oldData($data,'short_description','')}}
                        </textarea>
                        @include('admin.elements.validate_error', ['message' => $errors->first('short_description')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Nội dung</label>
                    <div class="col-lg-10 slide-element">
                        <textarea class="form-control" name="content" id="slide_content">{!!  oldData($data,'content','')!!}
                        </textarea>
                        @include('admin.elements.validate_error', ['message' => $errors->first('slide_content')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Thứ tự hiển thị<sup>*</sup></label>
                    <div class="col-lg-10 slide-element">
                        <input type="number" class="form-control" name="sort" id="sort_slide"
                               value="{{oldData($data,'sort','')}}"/>
                        @include('admin.elements.validate_error', ['message' => $errors->first('sort')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Vị trí hiển thị<sup>*</sup></label>
                    <div class="col-lg-5 user-element">
                        <select style="width:100%;" name="category_id" class="form-control">
                            @php($categoryId = oldData($data, 'category_id', 0))
                            <option value="">Chọn vị trí</option>
                            @foreach($category as $key => $value)
                                <option {{($categoryId == $key) ? 'selected' : ''}} value="{{$key}}"/>{{$value}}
                            @endforeach
                        </select>
                        @include('admin.elements.validate_error', ['message' => $errors->first('category_id')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Trạng thái<sup>*</sup></label>
                    <div class="col-lg-5 user-element">
                        <select style="width:100%;" name="status" class="form-control form-status">
                            @php($status = oldData($data, 'status', 0))
                            @foreach($slideStatus as $key => $value)
                                <option {{($status == $key) ? 'selected' : ''}} value="{{$key}}"/>{{$value}}
                            @endforeach
                        </select>
                        @include('admin.elements.validate_error', ['message' => $errors->first('status')])
                    </div>
                </div>
                <div class="form-group upload-slide">
                    <label class="col-lg-2 control-label">Ảnh đại diện</label>
                    <div class="col-lg-10 no_pad_left">
                        <div class="col-lg-4 img-slide">
                            @php($avatar = oldData($data, 'image',''))
                            @if(empty($avatar))
                                <img src="{{asset('/images/upload-default.png')}}"
                                     id="image_url">
                            @else
                                <img src="{{asset('/upload/slide/'.$avatar)}}"
                                     id="image_url">
                            @endif
                            @include('admin.elements.validate_error', ['message' => $errors->first('image')])
                        </div>
                        <div class="col-lg-8 notification-element">
                            <label for="post_file" class="btn btn-primary">Chọn ảnh</label>
                            <input type="file" id="post_file" name="post_file" class="upload-input"
                                   style="visibility:hidden;"/>
                            <input type="hidden" name="image" id="image" class="upload-input" value="{{$avatar}}"
                                   style="visibility:hidden;"/>

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">ALT Seo<sup></sup></label>
                    <div class="col-lg-10 user-element">
                        <input type="text" class="form-control" name="img_seo" id="slide_img_seo"
                               value="{{oldData($data,'img_seo','')}}"
                               placeholder="Nhập alt seo ảnh"/>
                        @include('admin.elements.validate_error', ['message' => $errors->first('img_seo')])
                    </div>
                </div>

                <div class="form-group row" style="margin-top: 40px;">
                    <div class="col-sm-2 col-form-label"></div>
                    <div class="col-sm-10 button-submit">
                        <button class="btn btn-warning btn-reset" type="button" onclick="location.reload();">
                            Nhập lại
                        </button>
                        <button class="btn btn-success submit-slide" type="submit">
                            Lưu
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection
@php($urlUpload = !empty($id) ? '../uploadFile' : '../slide/uploadFile')
@section('js')
    <script>
        CKEDITOR.replace('slide_content');
        $('input[type="file"]').change(function () {
            var formData = new FormData();
            var fileToUpload = $('#post_file').prop('files')[0];
            formData.append("fileToUpload", fileToUpload);
            $.ajax({
                url: '{{$urlUpload}}',
                method: 'POST',
                data: formData,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                processData: false,
                contentType: false,
                cache: false,
                success: function (result) {
                    $('.img-slide img').attr("src", "{{asset('upload/slide')}}/" + result);
                    $('#image').val(result);
                }
            });
        });
        $('#slide_title').keyup(function () {
            var titleSeo = ChangeToSlug($(this).val());
            $('#slide_title_seo').val(titleSeo);
        })
    </script>
@endsection