@extends('admin.layouts.master')

@section('content')
    <section class="breadcrumbs">
        @if(session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
    </section>
    <div class="header mt-md-5">
        <div class="page-breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="/">Trang chủ</a>
                </li>
                <li><a href="{{route('admin.slide.index')}}">Danh sách slide</a></li>
                <li class="active">Chi tiết slide</li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <div class="row view-notifications">
            <div class="col-lg-12 col-sm-12 col-xs-12">
                <div class="widget radius-bordered">
                    <div class="widget-header">
                        <span class="widget-caption">Chi tiết slide "<strong
                                    style="color:red;">{{$data->title}}</strong>"</span>
                        <a href="{{ \Illuminate\Support\Facades\URL::previous() }}" class="btn btn-warning back-button"><i
                                    class="fa fa-reply"></i>Quay
                            lại</a>
                    </div>
                    <div class="widget-body ">

                        <table class="table table-striped table-hover table-bordered" id="slide-detail">
                            <tbody>
                            @if (!empty($data))
                                <tr>
                                    <td class="detail-title">Id</td>
                                    <td>{{$data['id']}}</td>
                                </tr>
                                <tr>
                                    <td class="detail-title">Tiêu đề</td>
                                    <td>{{$data['title']}}</td>
                                </tr>
                                <tr>
                                    <td class="detail-title">Tiêu đề seo</td>
                                    <td>{{$data['title_seo']}}</td>
                                </tr>
                                <tr>
                                    <td class="detail-title">Link url</td>
                                    <td>{{$data['url']}}</td>
                                </tr>
                                <tr>
                                    <td class="detail-title">Link facebook</td>
                                    <td>{{$data['facebook_url']}}</td>
                                </tr>
                                <tr>
                                    <td class="detail-title">Link instal</td>
                                    <td>{{$data['instal_url']}}</td>
                                </tr>
                                <tr>
                                    <td class="detail-title">Mô tả ngắn</td>
                                    <td>{{$data['short_description']}}</td>
                                </tr>
                                <tr>
                                    <td class="detail-title">Nội dung</td>
                                    <td>{!! nl2br(e(strip_tags(html_entity_decode($data['content'])))) !!}</td>
                                </tr>
                                <tr>
                                    <td class="detail-title">Ảnh đại diện</td>
                                    <td>
                                        @if ($data['image'] =='')
                                            <img src="{{asset('/images/upload-default.png')}}">
                                        @else
                                            <img src="{{asset('/upload/slide')}}/{{$data['image']}}">
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="detail-title">Người tạo</td>
                                    <td>{{$data['user_name']}}</td>
                                </tr>
                                <tr>
                                    <td class="detail-title">Ngày tạo</td>
                                    <td>{{$data['created_at']}}</td>
                                </tr>
                                <tr>
                                    <td class="detail-title">Trạng thái</td>
                                    <td class="status">
                                        @if($data['status'] == config('const.status.enable'))
                                            <i class="fas fa-check-circle"></i>
                                        @else
                                            <i class="fas fa-times-circle"></i>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

