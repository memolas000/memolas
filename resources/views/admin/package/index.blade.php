@extends('admin.layouts.master')

@section('content')
    <section class="breadcrumbs">
        @if(session('success')) <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
    </section>
    <div class="header mt-md-5">
        <div class="page-breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="/">Trang chủ</a>
                </li>
                <li><a href="{{route('admin.package.index')}}">Quản lý gói cước</a></li>
                <li class="active">Danh sách gói cước</li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <h1 class="text-center mb-5 title">Danh sách package</h1>
        <div class="table-toolbar package">
            <a id="editabledatatable_new" href="{{route('admin.package.create')}}" class="btn btn-default btn-add-new">
                Thêm mới
            </a>

            <table class="table table-striped table-hover table-bordered" id="package-table">
                <thead>
                <tr role="row">
                    <th>STT</th>
                    <th>Tên gói</th>
                    <th>Ảnh đại diện</th>
                    <th>Giá gói</th>
                    <th>Tỷ lệ memolas</th>
                    <th>Người tạo</th>
                    <th>Ngày tạo</th>
                    <th>Trạng thái</th>
                    <th style="display: none;"></th>
                    <th></th>
                </tr>
                </thead>

                <tbody>
                @php( $stt = 0)
                @if (!empty($data))
                    @foreach($data as $k => $row)
                        @php( $stt ++)
                        <tr class="package-{{$row->id}}">
                            <td><?php echo $stt?></td>
                            <td class="text-left"><a
                                        href="{{route('admin.package.show',['id'=> $row->id])}}">{{$row->name}}</a></td>
                            <td class="avatar">
                                @if ($row->avatar =='')
                                    <img src="{{asset('/images/upload-default.png')}}">
                                @else
                                    <img src="{{asset('/upload/package')}}/{{$row->avatar}}">
                                @endif
                            </td>
                            <td class="text-center">
                                {{$row->price_fix}}
                            </td>
                            <td class="text-center">
                                {{$row->ratio_memolas}}
                            </td>
                            <td class="text-left"><a
                                        href="{{route('admin.package.show',['id'=> $row->id])}}">{{$row->user_name}}</a>
                            </td>
                            <td class="text-left"><a
                                        href="{{route('admin.package.show',['id'=> $row->id])}}">{{$row->created_at}}</a>
                            </td>
                            <td class="status text-center">
                                @if($row->status==config('const.status.enable'))
                                    <a><i class="fas fa-check-circle"></i></a>
                                @else
                                    <a><i class="fas fa-times-circle"></i></a>
                                @endif
                            </td>
                            <td style="display: none;">
                                @if($row->status==config('const.status.enable'))
                                    1
                                @else
                                    0
                                @endif

                            </td>
                            <td class="action">
                                <a href="{{route('admin.package.show',['id'=> $row->id])}}"
                                   class="btn btn-success btn-xs view"><i class="fa fa-eye"></i>
                                </a>

                                <a href="{{route('admin.package.edit',['id' => $row->id])}}"
                                   class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i>
                                </a>

                                <a href="javascript:showModal('{{$row->id}}')"
                                   class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i>
                                </a>
                                @include('admin.elements.modal',['modal'=>'modalConfirmDelete', 'id' => $row->id,'url' => 'package/'.$row->id,'title'=>$row->title])
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10">Không có kết quả
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
