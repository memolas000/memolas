@extends('admin.layouts.master')

@section('content')
    <section class="breadcrumbs">
        @if(session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
    </section>
    <div class="header mt-md-5">
        <div class="page-breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="/">Trang chủ</a>
                </li>
                <li><a href="{{route('admin.package.index')}}">Danh sách package</a></li>
                <li class="active">Chi tiết package</li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <div class="row view-notifications">
            <div class="col-lg-12 col-sm-12 col-xs-12">
                <div class="widget radius-bordered">
                    <div class="widget-header">
                        <span class="widget-caption">Chi tiết package "<strong
                                    style="color:red;">{{$data->name}}</strong>"</span>
                        <a href="{{ \Illuminate\Support\Facades\URL::previous() }}" class="btn btn-warning back-button"><i
                                    class="fa fa-reply"></i>Quay
                            lại</a>
                    </div>
                    <div class="widget-body ">

                        <table class="table table-striped table-hover table-bordered" id="package-detail">
                            <tbody>
                            @if (!empty($data))
                                <tr>
                                    <td class="detail-title">Id</td>
                                    <td>{{$data['id']}}</td>
                                </tr>
                                <tr>
                                    <td class="detail-title">Tên</td>
                                    <td>{{$data['name']}}</td>
                                </tr>
                                <tr>
                                    <td class="detail-title">Nội dung</td>
                                    <td>{!! nl2br(e(strip_tags(html_entity_decode($data['description'])))) !!}</td>
                                </tr>
                                <tr>
                                    <td class="detail-title">Thông số giá</td>
                                    <td><table style="width: 400px;">
                                            <tbody>
                                            <tr>
                                                <td>Giá cứng</td>
                                                <td>{{$data['price_fix'].'K'}}</td>
                                            </tr>
                                            @foreach($option as $key => $value)
                                                <?php
                                                    $price = isset($optionPackage[$value['id']]['option_price']) ? $optionPackage[$value['id']]['option_price'] : 0;
                                                ?>
                                                <?php
                                                    $minPrice = isset($optionPackage[$value['id']]['option_min_price']) ? $optionPackage[$value['id']]['option_min_price'] : 0;
                                                ?>
                                                <tr>
                                                    <td>{{$value['name']}}</td>
                                                    <td>{{$price.'K'}}</td>
                                                    <td>Min Price</td>
                                                    <td>{{$minPrice.'K'}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="detail-title">Ảnh đại diện</td>
                                    <td class="avatar-detail">
                                        @if ($data['avatar'] =='')
                                            <img src="{{asset('/images/upload-default.png')}}">
                                        @else
                                            <img src="{{asset('/upload/package')}}/{{$data['avatar']}}">
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="detail-title">Người tạo</td>
                                    <td>{{$data['user_name']}}</td>
                                </tr>
                                <tr>
                                    <td class="detail-title">Ngày tạo</td>
                                    <td>{{$data['created_at']}}</td>
                                </tr>
                                <tr>
                                    <td class="detail-title">Trạng thái</td>
                                    <td class="status">
                                        @if($data['status'] == config('const.status.enable'))
                                            <i class="fas fa-check-circle"></i>
                                        @else
                                            <i class="fas fa-times-circle"></i>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

