@extends('admin.layouts.master')
{{--@php(dd(session()->getOldInput()));--}}
@section('content')
    <section class="breadcrumbs">
        @if(session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
    </section>
    <div class="header mt-md-5">
        <div class="page-breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="/">Trang chủ</a>
                </li>
                <li><a href="{{route('admin.package.index')}}">Danh sách gói cước</a></li>
                <li class="active">{{!empty($id) ? 'Chỉnh sửa package' : 'Tạo mới'}}</li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <h1 class="text-left mb-5 title">{{!empty($id) ? 'Chỉnh sửa gói cước số '.$id : 'Tạo mới gói cước'}}</h1>
        <div class="row">
            <form id="package-add-form" method="post" class="form-horizontal"
                  action="{{!empty($id) ? route("admin.package.update",[$id]) : route("admin.package.store")}}">
                {{ csrf_field() }}
                {{ !empty($id) ? method_field('PATCH') :'' }}
                <div class="form-group">
                    <label class="col-lg-2 control-label">Tên</label>
                    <div class="col-lg-10 package-element">
                        <input type="text" class="form-control" name="name" id="package_title"
                               value="{{oldData($data,'name','')}}"
                               placeholder="Nhập tiêu đề"/>
                        @include('admin.elements.validate_error', ['message' => $errors->first('name')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Mô tả</label>
                    <div class="col-lg-10 package-element">
                        <textarea class="form-control" name="description" id="package_description">{!! oldData($data,'description','')!!}
                        </textarea>
                        @include('admin.elements.validate_error', ['message' => $errors->first('description')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Cài đặt option<sup>*</sup></label>
                    <div class="col-lg-10 package-element">
                        <table class="table table-bordered table-option">
                            <tbody>
                            @foreach($option as $key => $value)
                                <?php
                                if (isset(old('price')[$value['id']])) {
                                    $price = old('price')[$value['id']];
                                } else {
                                    $price = isset($optionPackage[$value['id']]['option_price']) ? $optionPackage[$value['id']]['option_price'] : 0;
                                }
                                if (isset(old('min_price')[$value['id']])) {
                                    $minPrice = old('min_price')[$value['id']];
                                } else {
                                    $minPrice = isset($optionPackage[$value['id']]['option_min_price']) ? $optionPackage[$value['id']]['option_min_price'] : 0;
                                }
                                ?>
                                <tr>
                                    <td><p class="label-option">{{$value['name']}}</p></td>
                                    <td><input type="text" class="form-control" name="price[{{$value['id']}}]"
                                               value="{{$price}}">
                                    </td>
                                    <td><input type="text" class="form-control" name="min_price[{{$value['id']}}]"
                                               value="{{$minPrice}}">
                                    </td>
                                    {{--                                    <td><a href="javascript:removeOption();" class="remove-option"><i class="fa fa-trash"></i></a></td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Giá cứng<sup>*</sup></label>
                    <div class="col-lg-10 package-element">
                        <input type="text" class="form-control" name="price_fix" id="price_fix_package"
                               value="{{oldData($data,'price_fix','')}}"/>
                        @include('admin.elements.validate_error', ['message' => $errors->first('price_fix')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Tỷ lệ memolas<sup>*</sup></label>
                    <div class="col-lg-10 package-element">
                        <input type="text" class="form-control" name="ratio_memolas" id="ratio_memolas_package"
                               value="{{oldData($data,'ratio_memolas','')}}"/>
                        @include('admin.elements.validate_error', ['message' => $errors->first('ratio_memolas')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Trạng thái<sup>*</sup></label>
                    <div class="col-lg-5 user-element">
                        <select style="width:100%;" name="status" class="form-control form-status">
                            @php($status = oldData($data, 'status', 0))
                            @foreach($packageStatus as $key => $value)
                                <option {{($status == $key) ? 'selected' : ''}} value="{{$key}}"/>{{$value}}
                            @endforeach
                        </select>
                        @include('admin.elements.validate_error', ['message' => $errors->first('status')])
                    </div>
                </div>
                <div class="form-group upload-package">
                    <label class="col-lg-2 control-label">Ảnh đại diện</label>
                    <div class="col-lg-10 no_pad_left">
                        <div class="col-lg-4 img-package">
                            @php($avatar = oldData($data, 'avatar',''))
                            @if(empty($avatar))
                                <img src="{{asset('/images/upload-default.png')}}"
                                     id="image_url">
                            @else
                                <img src="{{asset('/upload/package/'.$avatar)}}"
                                     id="image_url">
                            @endif
                            @include('admin.elements.validate_error', ['message' => $errors->first('avatar')])
                        </div>
                        <div class="col-lg-8 notification-element">
                            <label for="post_file" class="btn btn-primary">Chọn ảnh</label>
                            <input type="file" id="post_file" name="post_file" class="upload-input"
                                   style="visibility:hidden;"/>
                            <input type="hidden" name="avatar" id="avatar" class="upload-input" value="{{$avatar}}"
                                   style="visibility:hidden;"/>

                        </div>
                    </div>
                </div>

                <div class="form-group row" style="margin-top: 40px;">
                    <div class="col-sm-2 col-form-label"></div>
                    <div class="col-sm-10 button-submit">
                        <button class="btn btn-warning btn-reset" type="button" onclick="location.reload();">
                            Nhập lại
                        </button>
                        <button class="btn btn-success submit-package" type="submit">
                            Lưu
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection
@php($urlUpload = !empty($id) ? '../uploadFile' : '../package/uploadFile')
@section('js')
    <script>
        CKEDITOR.replace('package_description');
        $('input[type="file"]').change(function () {
            var formData = new FormData();
            var fileToUpload = $('#post_file').prop('files')[0];
            formData.append("fileToUpload", fileToUpload);
            $.ajax({
                url: '{{$urlUpload}}',
                method: 'POST',
                data: formData,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                processData: false,
                contentType: false,
                cache: false,
                success: function (result) {
                    $('.img-package img').attr("src", "{{asset('upload/package')}}/" + result);
                    $('#avatar').val(result);
                }
            });
        });
        $('#package_title').keyup(function () {
            var titleSeo = ChangeToSlug($(this).val());
            $('#package_title_seo').val(titleSeo);
        })
    </script>
@endsection