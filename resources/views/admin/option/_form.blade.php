@extends('admin.layouts.master')

@section('content')
    <section class="breadcrumbs">
        @if(session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
    </section>
    <div class="header mt-md-5">
        <div class="page-breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="/">Trang chủ</a>
                </li>
                <li><a href="{{route('admin.option.index')}}">Danh sách option</a></li>
                <li class="active">{{!empty($id) ? 'Chỉnh sửa option' : 'Tạo mới'}}</li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <h1 class="text-left mb-5 title">{{!empty($id) ? 'Chỉnh sửa option số '.$id : 'Tạo mới option'}}</h1>
        <div class="row">
            <form id="option-add-form" method="post" class="form-horizontal"
                  action="{{!empty($id) ? route("admin.option.update",[$id]) : route("admin.option.store")}}">
                {{ csrf_field() }}
                {{ !empty($id) ? method_field('PATCH') :'' }}
                <div class="form-group">
                    <label class="col-lg-2 control-label">Tên</label>
                    <div class="col-lg-10 option-element">
                        <input type="text" class="form-control" name="name" id="option_name"
                               value="{{oldData($data,'name','')}}"
                               placeholder="Nhập tên"/>
                        @include('admin.elements.validate_error', ['message' => $errors->first('name')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Mô tả</label>
                    <div class="col-lg-10 option-element">
                        <textarea class="form-control" name="description" id="option_description">{!!  oldData($data,'description','')!!}
                        </textarea>
                        @include('admin.elements.validate_error', ['message' => $errors->first('description')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Thể loại<sup>*</sup></label>
                    <div class="col-lg-5 user-element">
                        <select style="width:100%;" name="type" class="form-control form-status">
                            @php($type = oldData($data, 'type', 0))
                            @foreach($optionType as $key => $value)
                                <option {{($type == $key) ? 'selected' : ''}} value="{{$key}}"/>{{$value}}
                            @endforeach
                        </select>
                        @include('admin.elements.validate_error', ['message' => $errors->first('type')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Trạng thái<sup>*</sup></label>
                    <div class="col-lg-5 user-element">
                        <select style="width:100%;" name="status" class="form-control form-status">
                            @php($status = oldData($data, 'status', 0))
                            @foreach($optionStatus as $key => $value)
                                <option {{($status == $key) ? 'selected' : ''}} value="{{$key}}"/>{{$value}}
                            @endforeach
                        </select>
                        @include('admin.elements.validate_error', ['message' => $errors->first('status')])
                    </div>
                </div>

                <div class="form-group row" style="margin-top: 40px;">
                    <div class="col-sm-2 col-form-label"></div>
                    <div class="col-sm-10 button-submit">
                        <button class="btn btn-warning btn-reset" type="button" onclick="location.reload();">
                            Nhập lại
                        </button>
                        <button class="btn btn-success submit-option" type="submit">
                            Lưu
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection
@section('js')
    <script>
        CKEDITOR.replace('option_description');
    </script>
@endsection