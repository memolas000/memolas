<div class="modal fade" id="modalConfirmDelete_{{$id}}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col">
                            <h4 class="card-header-title" id="exampleModalCenterTitle">
                                Xóa bài viết
                            </h4>
                        </div>
{{--                        <div class="col-auto">--}}
{{--                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                                <span aria-hidden="true">&times;</span>--}}
{{--                            </button>--}}
{{--                        </div>--}}
                    </div>
                </div>
                <div class="card-body" align="center">
                    <h2>Bạn có chắc muốn xóa {{$title}}?</h2>
                </div>
                <div class="card-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">Hủy
                    </button>
                    &nbsp;
                    <button onclick="deleteElement('{{ $id }}' , '{{ $url }}', '{{ $title }}')" type="button"
                            class="btn btn-primary">OK
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>


