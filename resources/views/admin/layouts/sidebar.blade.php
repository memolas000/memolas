<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.dashboard') }}"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarPosts" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarPost">
                        <i class="menu-icon fa fa-book"></i> Quản lý bài viết
                    </a>
                    <div class="collapse" id="sidebarPosts">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item"><a class="nav-link" href="{{ route('admin.posts.index') }}">Danh sách bài viết</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('admin.posts.create') }}">Tạo mới</a></li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarSlide" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarSlide">
                        <i class="menu-icon fa fa-images" aria-hidden="true"></i>Quản lý slide
                    </a>
                    <div class="collapse" id="sidebarSlide">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item"><a class="nav-link" href="{{ route('admin.slide.index') }}">Danh sách slide</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('admin.slide.create') }}">Tạo mới</a></li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarPackage" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarPackage">
                        <i class="menu-icon fa fa-clone"></i>Quản lý gói cước
                    </a>
                    <div class="collapse" id="sidebarPackage">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item"><a class="nav-link" href="{{ route('admin.package.index') }}">Danh sách gói cước</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('admin.package.create') }}">Tạo mới</a></li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarOption" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarOption">
                        <i class="menu-icon fa fa-cog"></i>Quản lý option
                    </a>
                    <div class="collapse" id="sidebarOption">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item"><a class="nav-link" href="{{ route('admin.option.index') }}">Danh sách option</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('admin.option.create') }}">Tạo mới</a></li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarMember" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarMember">
                        <i class="menu-icon fa fa-id-card"></i>Quản lý thành viên
                    </a>
                    <div class="collapse" id="sidebarMember">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item"><a class="nav-link" href="{{ route('admin.member.index') }}">Danh sách thành viên</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('admin.member.create') }}">Tạo mới</a></li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarCategoryBlog" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarCategoryBlog">
                        <i class="menu-icon fa fa-id-card"></i>Quản lý loại blog
                    </a>
                    <div class="collapse" id="sidebarCategoryBlog">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item"><a class="nav-link" href="{{ route('admin.category-blog.index') }}">Danh sách loại blog</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('admin.category-blog.create') }}">Tạo mới</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</aside>
