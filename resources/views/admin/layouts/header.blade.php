<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Admin Memolas</title>
    <link rel="stylesheet" href="{{ secure_asset('css/app.css') }}">
    <link href="{{asset('css/admin/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/admin/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ secure_asset('css/admin/style.css') }}">
    <link rel="stylesheet" href="{{ secure_asset('css/admin/main.css') }}">
    @yield('css')
    <link rel="apple-touch-icon" href="{{ secure_asset('images/logo1.png') }}">
    <link rel="shortcut icon" href="{{ secure_asset('images/logo1.png') }}">
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

    <script src="{{asset('js/admin/jquery.min.js')}}"></script>
    <script src="{{asset('js/admin/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/admin/datatable/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/admin/datatable/ZeroClipboard.js')}}"></script>
    <script src="{{asset('js/admin/datatable/dataTables.tableTools.min.js')}}"></script>
    <script src="{{asset('js/admin/datatable/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('js/laroute.js')}}"></script>
    <script src="//cdn.ckeditor.com/4.8.0/full-all/ckeditor.js"></script>
    <script src="{{asset('js/admin/System.js')}}"></script>
</head>
