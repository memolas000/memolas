@extends('admin.layouts.master')

@section('content')
    <section class="breadcrumbs">
        @if(session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
    </section>
    <div class="header mt-md-5">
        <div class="page-breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="/">Trang chủ</a>
                </li>
                <li><a href="{{route('admin.member.index')}}">Danh sách thành viên</a></li>
                <li class="active">{{!empty($id) ? 'Chỉnh sửa member' : 'Tạo mới'}}</li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <h1 class="text-left mb-5 title">{{!empty($id) ? 'Chỉnh sửa thành viên số '.$id : 'Tạo mới thành viên'}}</h1>
        <div class="row">
            <form id="member-add-form" method="post" class="form-horizontal"
                  action="{{!empty($id) ? route("admin.member.update",[$id]) : route("admin.member.store")}}">
                {{ csrf_field() }}
                {{ !empty($id) ? method_field('PATCH') :'' }}
                <div class="form-group">
                    <label class="col-lg-3 control-label">Tên<sup>*</sup></label>
                    <div class="col-lg-9 member-element">
                        <input type="text" class="form-control" name="name" id="member_name"
                               value="{{oldData($data,'name','')}}"
                               placeholder="Nhập tên"/>
                        @include('admin.elements.validate_error', ['message' => $errors->first('name')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Thông tin</label>
                    <div class="col-lg-9 member-element">
                        <textarea class="form-control" name="infor" id="infor_description">{!!  oldData($data,'infor','')!!}
                        </textarea>
                        @include('admin.elements.validate_error', ['message' => $errors->first('infor')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Chức vụ<sup>*</sup></label>
                    <div class="col-lg-9 member-element">
                        <input type="text" class="form-control" name="job" id="job_name"
                               value="{{oldData($data,'job','')}}"
                               placeholder="Nhập chức vụ"/>
                        @include('admin.elements.validate_error', ['message' => $errors->first('job')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Link facebook</label>
                    <div class="col-lg-9 member-element">
                        <input type="text" class="form-control" name="facebook_url" id="member_facebook_url"
                               value="{{oldData($data,'facebook_url','')}}"
                               placeholder="Nhập link"/>
                        @include('admin.elements.validate_error', ['message' => $errors->first('facebook_url')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Link instagram</label>
                    <div class="col-lg-9 member-element">
                        <input type="text" class="form-control" name="insta_url" id="member_insta_url"
                               value="{{oldData($data,'insta_url','')}}"
                               placeholder="Nhập link"/>
                        @include('admin.elements.validate_error', ['message' => $errors->first('insta_url')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Thứ tự hiển thị<sup>*</sup></label>
                    <div class="col-lg-9 slide-element">
                        <input type="number" class="form-control" name="sort" id="sort_slide"
                               value="{{oldData($data,'sort','')}}"/>
                        @include('admin.elements.validate_error', ['message' => $errors->first('sort')])
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Trạng thái<sup>*</sup></label>
                    <div class="col-lg-5 user-element">
                        <select style="width:100%;" name="status" class="form-control form-status">
                            @php($status = oldData($data, 'status', 0))
                            @foreach($memberStatus as $key => $value)
                                <option {{($status == $key) ? 'selected' : ''}} value="{{$key}}"/>{{$value}}</option>
                            @endforeach
                        </select>
                        @include('admin.elements.validate_error', ['message' => $errors->first('status')])
                    </div>
                </div>
                <div class="form-group upload-member">
                    <label class="col-lg-3 control-label">Ảnh đại diện<sup>*</sup></label>
                    <div class="col-lg-9 no_pad_left">
                        <div class="col-lg-4 img-member">
                            @php($avatar = oldData($data, 'avatar',''))
                            @if(empty($avatar))
                                <img src="{{asset('/images/upload-default.png')}}"
                                     id="image_url">
                            @else
                                <img src="{{asset('/upload/member/'.$avatar)}}"
                                     id="image_url">
                            @endif
                            @include('admin.elements.validate_error', ['message' => $errors->first('avatar')])
                        </div>
                        <div class="col-lg-8 notification-element">
                            <label for="post_file" class="btn btn-primary">Chọn ảnh</label>
                            <input type="file" id="post_file" name="post_file" class="upload-input"
                                   style="visibility:hidden;"/>
                            <input type="hidden" name="avatar" id="avatar" class="upload-input" value="{{$avatar}}"
                                   style="visibility:hidden;"/>

                        </div>
                    </div>
                </div>

                <div class="form-group row" style="margin-top: 40px;">
                    <div class="col-sm-3 col-form-label"></div>
                    <div class="col-sm-9 button-submit">
                        <button class="btn btn-warning btn-reset" type="button" onclick="location.reload();">
                            Nhập lại
                        </button>
                        <button class="btn btn-success submit-member" type="submit">
                            Lưu
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection
@php($urlUpload = !empty($id) ? '../uploadFile' : '../member/uploadFile')
@section('js')
    <script>
        $('input[type="file"]').change(function () {
            var formData = new FormData();
            var fileToUpload = $('#post_file').prop('files')[0];
            formData.append("fileToUpload", fileToUpload);
            $.ajax({
                url: '{{$urlUpload}}',
                method: 'POST',
                data: formData,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                processData: false,
                contentType: false,
                cache: false,
                success: function (result) {
                    $('.img-member img').attr("src", "{{asset('upload/member')}}/" + result);
                    $('#avatar').val(result);
                }
            });
        });
    </script>
@endsection